#encoding: UTF-8
from django.shortcuts import render_to_response
from django.utils import simplejson
from django.utils.encoding import smart_str, smart_unicode
from django.template import RequestContext
from django.contrib import messages
from django.contrib.sessions.backends.db import SessionStore
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth import login as auth_login
from django.contrib.auth.models import User, Group
from django.http import HttpResponseRedirect, HttpResponse
from farmacia.models import Aplicacion, MenuFirstLevel, MenuSecondLevel, MenuThirdLevel
from farmacia.models import Producto
from farmacia.forms import LoginForm
from dataview import getDataIngreso, getDataDetalle, getDataStock, getDataStockGranel
from sislog import logear

import datetime
import time
import json
import traceback

def ingreso(request):
	_Ingreso = getDataIngreso(request)
	_Detalle = getDataDetalle(request)
	_Stock = getDataStock(request)
	_StockGranel = getDataStockGranel(request)
	if request.method == 'POST':
		#for x in request.POST:
		#	print x
		#	print request.POST[x]
		
		i_Productos = []
		i_Detalle = []
		i_Total = 0
		i_NoFound = []
		for x in request.POST['productos'].split(','):
			if x:
				try:
					if int(x.split('-')[0].split('o')[3]):
						granel = _StockGranel['Clase'].objects.get(id = x.split('-')[0].split('o')[3])
					else:
						granel = 0
					i_Productos.append([_Detalle['Unidad'].objects.get(id = x.split('-')[0].split('o')[0]),
										int(x.split('-')[1]),
										_Stock['Clase'].objects.get(id = x.split('-')[0].split('o')[1]),
										granel
										])
				except Exception, e:
					error = traceback.extract_stack()[-1]
					print (str(datetime.datetime.now()) + " - " + ", ".join([type(e).__name__, str(error[0]), str(error[2]), str(error[1])]))
					i_NoFound.append(x.split('-'))
		for x in i_Productos:
			xf = _Detalle['Formulario']({'producto': x[0].id, 'cantidad': x[1], 'contado' : 'F'})
			if xf.is_valid():
				try:
					xf.save()
					if x[3]:
						x[3].cantidad_inversa = x[3].cantidad_inversa + x[1]
						x[3].save()
						i_Total += x[3].precio
					else:
						x[2].cantidad_inversa = x[2].cantidad_inversa + x[1]
						x[2].save()
						i_Total += x[2].precio
					i_Detalle.append(xf.save().id)
				except Exception, e:
					error = traceback.extract_stack()[-1]
					print (str(datetime.datetime.now()) + " - " + ", ".join([type(e).__name__, str(error[0]), str(error[2]), str(error[1])]))
		xf = _Ingreso['Formulario']({
									'detalle' : i_Detalle,
									'ruc' : '0',
									'cancelado' : 'F',
									'contado' : 'F',
									'monto' : i_Total,
									'igv' : 0,
									'boleta' : '0',
									'cliente' : 'Desconocido',
									'emision' : datetime.datetime.now(),
									'usuariox' : request.user.id
								})
		if xf.is_valid():
			#try:
			xf.save()
			#except:
			#	print "Error"
		else:
			print xf.errors
		return HttpResponse(simplejson.dumps(_Ingreso['ToForceReturn']), mimetype='application/json')
		form = _Detalle['Formulario'](request.POST)
		if form.is_valid():
			try:
				form.save()
			except Exception, e:
				error = traceback.extract_stack()[-1]
				logear(str(datetime.datetime.now()) + " - " + ", ".join([type(e).__name__, str(error[0]), str(error[2]), str(error[1])]))
				if _Ingreso['Ajax']:
					return HttpResponse(simplejson.dumps(_Ingreso['ToForceReturn']), mimetype='application/json')
				else:
					return HttpResponseRedirect(_Ingreso['ToForceReturn'])
		else:
			messages.error(request, _Ingreso['MsgErrorForm'])
			if _Ingreso['Ajax']:
				return HttpResponse(simplejson.dumps(_Ingreso['ToForceReturn']), mimetype='application/json')
			else:
				return render_to_response(_Ingreso['Html'], _Ingreso['ErrorForm'], context_instance = RequestContext(request))
		
