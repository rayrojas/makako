# encoding:utf-8
from django import forms
from django.shortcuts import render_to_response
from django.utils import simplejson
from django.utils.encoding import smart_str, smart_unicode
from django.template import RequestContext
from django.contrib import messages
from django.contrib.sessions.backends.db import SessionStore
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth import login as auth_login
from django.contrib.auth.models import User, Group
from django.http import HttpResponseRedirect, HttpResponse
from views import getAplicacion, getMenuFirst, getMenuSecond, getThirdSecond, isLogin, okPermission
from farmacia.models import Laboratorio
from farmacia.forms import LaboratorioForm
from farmacia.models import Lote
from farmacia.forms import LoteForm
from farmacia.models import Componente
from farmacia.forms import ComponenteForm
from farmacia.models import Malestar
from farmacia.forms import MalestarForm
from farmacia.models import Stock
from farmacia.forms import StockForm
from farmacia.forms import StockFormEdit
from farmacia.models import Producto
from farmacia.models import FrontDesk
from farmacia.models import StockForFront
from farmacia.models import StockUnidadTemp
from farmacia.models import StockGranelOfTemp
from farmacia.forms import ProductoForm
from farmacia.forms import StockUnidadTempForm
from farmacia.forms import StockForFrontForm
from farmacia.forms import StockGranelOfTempForm
from sislog import logear
import datetime
import time
import json
import traceback

class ObjectWork:
	_Object = []
	_ObjectForm = []
	_Title = ""
	_Titles = ""
	_Path = ""
	_Html = ""
#maestro Compuestos
def RStock(request, codigo = ""):
	usuario = request.user
	if not (isLogin(usuario)) and (Usuariox.objects.all()):
		logear(str(datetime.datetime.now()) + " - RStock" + " - " + str(usuario) + " No inicio sesión.")
		return HttpResponseRedirect("/login/")
	if (okPermission(usuario.groups.all(), 1)):
		# Variables
		_Object = ObjectWork()
		_Object._Object = Stock
		_Object._ObjectForm = StockForm
		_Object._Title = "Stock por Mayor"
		_Object._Titles = "Stocks por Mayor"
		_Object._Path = "/administrator/farmacia/stock/"
		_Object._Html = "stocka.html"
		Generals = {
			'aplicacion' : getAplicacion(request = request),								# Trae el objeto Aplicación, consiste en datos generales del sistema
			'items' : getMenuFirst(usuario.groups.all()),				# Trae objetos que representan opciones de un menu principal, relativo a un usuario
			'menu' :  getMenuSecond(usuario.groups.all()),				# Trae objetos que representan opciones de un menu secundario, relativo a un usuario
			'titulo' : 'Formulario de ' + _Object._Titles,					# Titulo del formulario de registro
			'titulo_segundo' : "Lista de " + _Object._Titles,				# Titulo del listo de objetos
			'action' : _Object._Path,
			'objetos' : _Object._Object.objects.all(),	# Objetos que serán listados
			'suject' : [],												# Objeto que será editado
			'mensaje' : "",												# Mensaje de éxito o fracazo
			'Url' : request.get_full_path(),							# Dirección URL importante para el menu secundario
		}	
		if request.method == 'POST':
			# Todas las operaciones que sucedan a continuación se realizarán solo cuando se haga POST
			if codigo:
			# Si tiene código, significa que vamos a editar un registro
				try:
					Generals['suject'] = _Object._Object.objects.get(id = codigo)	# El objeto a editar a guardado en suject, que antes estaba vacio
					print request.POST
					form = _Object._ObjectForm(request.POST, instance = Generals['suject'])
					Generals['mensaje'] = "Se modificó satisfactoriamente. " + smart_str(Generals['suject'])	#La función smart_str exita errores de tildes
				except Exception, e:
					messages.error(request, _Object._Title + " a editar no ha sido encontrada.")
					error = traceback.extract_stack()[-1]
					logear(str(datetime.datetime.now()) + " - " + ", ".join([type(e).__name__, str(error[0]), str(error[2]), str(error[1])]))
					return HttpResponseRedirect(_Object._Path)
			else:
				# Nos indica que se trata de un registro nuevo
				form = _Object._ObjectForm(request.POST)
				print form
				Generals['mensaje'] = "Se registró satisfactoriamente."
			if form.is_valid():
				# Validamos los datos a guardar
				form.save()
				messages.success(request, Generals['mensaje'])
				return HttpResponseRedirect(_Object._Path)
			else:
				# Si los datos no son validos
				if codigo:
					# Si esque se trataba de un registro existente
					messages.error(request, "No se modificaron los datos.")
					codigo += "/"
				else:
					# Si esque se trataba de un nuevo registro
					messages.error(request, "No se registraron los datos.")
				# Preparamos las variables más los errores que tiene el form
				context = {
					'aplicacion' : Generals['aplicacion'],
					'items' : Generals['items'],
					'menu' :  Generals['menu'],
					'titulo' : Generals['titulo'],
					'titulo_segundo' : Generals['titulo_segundo'],	
					'action' : Generals['action'],
					'objetos' : Generals['objetos'],
					'Url' : Generals['Url'],
					'form' : form,
					'codigo' : codigo,
					'Url' : Generals['Url'],
				}
		else:
			# Cuando sea la primera vez que ingresa a esta vista
			if codigo:
				# Si posee codigo
				try:
					Generals['suject'] = _Object._Object.objects.get(id = codigo)
					form = StockFormEdit(instance = Generals['suject'], initial = { 'usuariox' : usuario.id })	# Pasando datos a un formulario
					codigo = str(codigo) + "/"	# Esta variable es para el action del formulario
					messages.info(request, ("Editando datos de " + smart_str(Generals['suject'])))
				except Exception, e:
					messages.error(request, _Object._Title + " a editar no ha sido encontrada.")
					error = traceback.extract_stack()[-1]
					logear(str(datetime.datetime.now()) + " - " + ", ".join([type(e).__name__, str(error[0]), str(error[2]), str(error[1])]))
					return HttpResponseRedirect(_Object._Path)
			else:
				# Mostrar un formulario vacio
				form = _Object._ObjectForm(initial = { 'usuariox' : usuario.id })
			context = {
					'aplicacion' : Generals['aplicacion'],			# Datos de la aplicación
					'items' : Generals['items'],					# Opciones del menú primario
					'menu' :  Generals['menu'],						# Opciones del menú secundario
					'titulo' : Generals['titulo'],					# Titulo del formulario
					'titulo_segundo' : Generals['titulo_segundo'],	# Titulo de la lista de Especialidades
					'action' : Generals['action'],					# Action del formulario
					'objetos' : Generals['objetos'],				# Especialidades
					'Url' : Generals['Url'],						# Indicador del menú secundario
					'codigo' : codigo,								# Código del objeto editado o caso contrario ""
					'form' : form,									# Formulario Vacio o con datos de una especialidad
			}
		return render_to_response(_Object._Html, context, context_instance = RequestContext(request))
	logear(str(datetime.datetime.now()) + " - especialidad_registro" + " - " + str("usuario") + " Denegación de servicios.")
	return HttpResponseRedirect('/')	
	
def RStockUnidadTemp(request, codigo):
	usuario = request.user
	if not (isLogin(usuario)) and (Usuariox.objects.all()):
		logear(str(datetime.datetime.now()) + " - RStock" + " - " + str(usuario) + " No inicio sesión.")
		return HttpResponseRedirect("/login/")
	if (okPermission(usuario.groups.all(), 1)):
		try:
			stock = Stock.objects.get(id = codigo)
		except:
			return HttpResponseRedirect('/administrator/dashboard/')
		Add = {}
		Add['titulo'] = 'Stock a Granel'
		Add['titulo_segundo'] = 'Lista de Stock a Granel'
		Add['stock'] = stock
		Add['objetos'] = StockUnidadTemp.objects.filter(stock = stock.id)
		Add['action'] = '/administrator/farmacia/stock/' + str(stock.id)+ '/granel/'
		if request.method == 'POST':

			form = StockUnidadTempForm(data = request.POST, maximo = 0)
			if form.is_valid():
					postc = request.POST.copy()
					cantidadreal = int(postc['cantidad'])
					postc['cantidad'] = str(cantidadreal * stock.cantidad_cu)
					form = StockUnidadTempForm(data = postc, maximo = 0)
					if form.is_valid():
						stock.cantidad_inversa += cantidadreal
						form.save()
						stock.save()
			else:
				messages.error(request, smart_str("No se registró"))
			return HttpResponseRedirect("/administrator/farmacia/stock/" + str(stock.id) + "/granel/")
		else:
			Add['form'] = StockUnidadTempForm(maximo = stock.get_total(), initial = { 'stock' : stock.id, 'usuariox' : usuario.id, 'cantidad' : stock.get_total() })
			context = {
					'aplicacion' : getAplicacion(request = request),			# Datos de la aplicación
					'items' : getMenuFirst(usuario.groups.all()),					# Opciones del menú primario
					'menu' :  getMenuSecond(usuario.groups.all()),						# Opciones del menú secundario
					'Url' : request.get_full_path(),						# Indicador del menú secundario
					'Add' : Add,
			}
		return render_to_response('stock.html', context, context_instance = RequestContext(request))
	logear(str(datetime.datetime.now()) + " - especialidad_registro" + " - " + str("usuario") + " Denegación de servicios.")
	return HttpResponseRedirect('/')	

def stock_local(request, local):
	usuario = request.user
	if not (isLogin(usuario)):
		return HttpResponseRedirect("/login/")
	if not getAplicacion(request, local) == False:
		if (okPermission(usuario.groups.all(), 1) or okPermission(usuario.groups.all(), 3)):	
			context = {
					'aplicacion' : getAplicacion(request = request),			# Datos de la aplicación
					'items' : getMenuFirst(usuario.groups.all()),					# Opciones del menú primario
					'menu' :  getMenuSecond(usuario.groups.all()),						# Opciones del menú secundario
					'Url' : request.get_full_path(),						# Indicador del menú secundario
					'Add' : Add,
			}
			'''
			stock = models.ForeignKey(Stock, verbose_name = "Stock")
			frontdesk = models.ForeignKey(FrontDesk, verbose_name = "FrontDesk")
			cantidad = models.IntegerField(verbose_name = "Cantidad de Unidades", help_text = "Ingrese la cantidad de unidades.", null = False, blank = False)
			cantidad_inversa 
			'''
			SFF = StockForFront.objects.filter( frontdesk = int(local) )
		
			return render_to_response('dashboard_local.html', variables, context_instance = RequestContext(request))		
	return HttpResponseRedirect("/login/")

def RStockasignacion(request, codigo):
	usuario = request.user
	if not (isLogin(usuario)):
		return HttpResponseRedirect("/login/")
	if (okPermission(usuario.groups.all(), 1)):
		try:
			stock = Stock.objects.get(id = codigo)
		except:
			MsgBox = "No se encontró el Stock."
			messages.error(request, MsgBox)
			return HttpResponseRedirect('/administrator/dashboard/')
		Add = {}
		Add['titulo'] = 'Asignación de Stock'
		Add['titulo_segundo'] = 'Lista de asignaciones de stock'		
		Add['objetos'] = StockForFront.objects.filter(stock = codigo)
		Add['stock'] = stock
		
		if request.method == "POST":
			form = StockForFrontForm(data = request.POST, maximo = 0)
			if form.is_valid():
				formregistred = form.save()
				formregistred.stock.cantidad_inversa += formregistred.cantidad
				formregistred.stock.save()
				text = smart_str("Se asignarón ") + str(formregistred.cantidad) + " unidades de " + smart_str(formregistred.stock.producto.producto) + " para " + smart_str(form.cleaned_data['frontdesk']) + "."
				messages.success(request, text)
			else:
				messages.error(request, "No se modificaron los datos.")
			return HttpResponseRedirect("/administrator/farmacia/stock/" + str(codigo) + "/asignacion/")

		Add['form'] = StockForFrontForm(maximo = stock.get_total(), initial = { 'stock' : stock.id, 'cantidad' : stock.get_total() })
		context = {
				'aplicacion' : getAplicacion(request = request),			# Datos de la aplicación
				'items' : getMenuFirst(usuario.groups.all()),					# Opciones del menú primario
				'menu' :  getMenuSecond(usuario.groups.all()),						# Opciones del menú secundario
				'Url' : request.get_full_path(),						# Indicador del menú secundario
				'Add' : Add,
		}
		return render_to_response('stock-asignacion.html', context, context_instance = RequestContext(request))		
	return HttpResponseRedirect("/login/")
'''
def get_bk_Stock(request, codigo):
	usuario = request.user
	if not (isLogin(usuario)):
		return HttpResponse(simplejson.dumps({"status" : "0", "msg" : "Inicie sesión."}), mimetype='application/json')
	if (okPermission(usuario.groups.all(), 1)):
		try:
			stock = Stock.objects.get(id = codigo)
		except Exception, e:
			error = traceback.extract_stack()[-1]
			logear(str(datetime.datetime.now()) + " - " + ", ".join([type(e).__name__, str(error[0]), str(error[2]), str(error[1])]))
			return HttpResponse(simplejson.dumps({"status" : "0", "msg" : "Error, datos no encontrados."}), mimetype='application/json')
		stock = {
			'stock' : smart_str(stock),
			'disponible' : str(stock.get_total()),
		}
		return HttpResponse(simplejson.dumps({"status" : "1", 'stock' : stock }), mimetype='application/json')
	return HttpResponse(simplejson.dumps({"status" : "0", "msg" : "Inicie sesión."}), mimetype='application/json')
'''
def RStockUnidadTempasignacion(request, codigo, granel):
	usuario = request.user
	if not (isLogin(usuario)):
		return HttpResponseRedirect("/login/")
	if (okPermission(usuario.groups.all(), 1)):

		try:
			stock = StockUnidadTemp.objects.get(id = granel)
		except:
			MsgBox = "No se encontró el Stock."
			messages.error(request, MsgBox)
			return HttpResponseRedirect('/administrator/dashboard/')
		Add = {}
		Add['titulo'] = 'Asignación de Stock'
		Add['titulo_segundo'] = 'Lista de asignaciones de stock'		
		Add['objetos'] = StockGranelOfTemp.objects.all()
		Add['stock'] = stock
		
		if request.method == "POST":
			form = StockGranelOfTempForm(data = request.POST, maximo = 0)
			if form.is_valid():
				formregistred = form.save()
				formregistred.stock.cantidad_inversa += formregistred.cantidad
				formregistred.stock.save()
				text = smart_str("Se asignarón ") + str(formregistred.cantidad) + " unidades de " + smart_str(formregistred.stock.stock.producto.producto) + " para " + smart_str(form.cleaned_data['frontdesk']) + "."
				messages.success(request, text)
			else:
				messages.error(request, "No se modificaron los datos.")
			return HttpResponseRedirect("/administrator/farmacia/stock/" + str(codigo) + "/granel/" + str(granel) + "/asignacion/")

		Add['form'] = StockGranelOfTempForm(maximo = stock.get_total(), initial = { 'stock' : stock.id, 'cantidad' : stock.get_total(), 'usuariox' : usuario.id })
		context = {
				'aplicacion' : getAplicacion(request = request),			# Datos de la aplicación
				'items' : getMenuFirst(usuario.groups.all()),					# Opciones del menú primario
				'menu' :  getMenuSecond(usuario.groups.all()),						# Opciones del menú secundario
				'Url' : request.get_full_path(),						# Indicador del menú secundario
				'Add' : Add,
		}
		return render_to_response('unidad-stock-asignacion.html', context, context_instance = RequestContext(request))		
	return HttpResponseRedirect("/login/")	
