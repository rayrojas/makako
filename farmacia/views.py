# -*- coding: UTF-8 -*-
from django.shortcuts import render_to_response
from django.utils import simplejson
from django.utils.encoding import smart_str, smart_unicode
from django.template import RequestContext
from django.contrib import messages
from django.contrib.sessions.backends.db import SessionStore
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth import login as auth_login
from django.contrib.auth.models import User, Group
from django.http import HttpResponseRedirect, HttpResponse
from farmacia.models import Aplicacion, MenuFirstLevel, MenuSecondLevel, MenuThirdLevel
from farmacia.models import Producto, Stock, StockGranel
from farmacia.forms import LoginForm
from farmacia.models import Venta
from farmacia.models import FrontDesk
from sislog import logear

#from sislog import logear
import datetime
import time
import json
import traceback
from django.core import serializers

class Configuracion():
	def __init__(self):
		self.IsDocumentado = True
		self.Aplicacion = Aplicacion.objects.all()[:1]

class ProductoFront():
	def __init__(self, id, presentacion, precio, nombre, nombrec, stock, lote, cantidad, granel = 0):
		if granel:
			self.id = str(id) + "o" + str(stock) + "o" + str(lote) + "o" + str(granel)
		else:
			self.id = str(id) + "o" + str(stock) + "o" + str(lote) + "o" + str(granel)
		self.stock = stock
		self.lote = lote
		self.cantidad = cantidad
		self.presentacion = presentacion
		self.precio = precio
		self.nombre = nombre
		self.nombrec = nombrec

def usuario(request):
	usuario = request.user
	if not (isLogin(usuario)):
		logear(str(datetime.datetime.now()) + " - usuario" + " - " + str(usuario) + " No inicio sesión.")
		return HttpResponseRedirect("/login/")
	if okPermission(usuario.groups.all(), 1):
		Add = {}
		Add['locales'] = FrontDesk.objects.all()
		context = {
			'aplicacion' : getAplicacion(request = request),
			'items' : getMenuFirst(usuario.groups.filter(id = 1)),
			'menu' :  getMenuSecond(usuario.groups.filter(id = 1)),
			'Add' : Add,
		}
		
		return render_to_response('usuario.html', context, context_instance = RequestContext(request))
	if okPermission(usuario.groups.all(), 2):
		Add = {}
		Add['locales'] = FrontDesk().get_FrontByUser(usuario)
		context = {
			'aplicacion' : getAplicacion(request = request),
			'items' : getMenuFirst(usuario.groups.all()),
			'menu' :  getMenuSecond(usuario.groups.all()),
			'Add' : Add,
		}
		return render_to_response('usuario.html', context, context_instance = RequestContext(request))		
	if okPermission(usuario.groups.all(), 3):
		Add = {}
		Add['locales'] = FrontDesk().get_FrontByUser(usuario)
		context = {
			'aplicacion' : getAplicacion(request = request),
			'items' : getMenuFirst(usuario.groups.all()),
			'menu' :  getMenuSecond(usuario.groups.all()),
			'Add' : Add,
		}
		return render_to_response('usuario.html', context, context_instance = RequestContext(request))				
	logear(str(datetime.datetime.now()) + " - usuario" + " - " + str(usuario) + " Denegación de servicios.")
	return HttpResponseRedirect("/usuario/")
		
def getAplicacion(request, local = 0):
	if local:
		try:
			local = FrontDesk.objects.get(id = local)
			if not okPermission(request.user.groups.all(), 1):
				if not str(request.user.id) in [str(x.id) for x in local.usuariox.all()]:
					return False
		except Exception, e:
			messages.error(request, "No se encuentra en un local.")
			error = traceback.extract_stack()[-1]
			logear(str(datetime.datetime.now()) + " - " + ", ".join([type(e).__name__, str(error[0]), str(error[2]), str(error[1])]))
			logout(request)
			return False
		ToConfig = {
			'local' : local,
		}
		return ToConfig
	return False

#jaquelin > jsantos@conitel.edu.pe

def getMenuFirst(g):
	items = []
	for grp in g:
		items.append(MenuFirstLevel.objects.filter(grupo = grp, disponible = True))
	return items

def getMenuSecond(g):
	items = {}
	items['Admin'] = []
	items['AdminFront'] = []
	items['Venta'] = []
	for grp in g:
		if grp.id == 1:
			items['Admin'].append(MenuSecondLevel.objects.filter(permisos = grp, disponible = True).order_by('orden'))
		if grp.id == 2:
			items['AdminFront'].append(MenuSecondLevel.objects.filter(permisos = grp, disponible = True).order_by('orden'))
		if grp.id == 3:
			items['Venta'].append(MenuSecondLevel.objects.filter(permisos = grp, disponible = True).order_by('orden'))
	return items

def getThirdSecond(g):
	items = []
	for grp in g:
		items.append(MenuThirdLevel.objects.filter(permisos = grp))
	return items

def isLogin(u):
	if u is not None and u.is_active:
		return True
	else:
		return False

def okPermission(u, patron):
	for uu in u:
		if uu.id == patron:
			return True
	return False

def login(request):
	if request.method == 'POST':
		form = LoginForm(request.POST)
		if form.is_valid():
			username = form.cleaned_data['username']
			passwd = form.cleaned_data['password']
			user = authenticate(username = username, password = passwd)
			if user is not None:
				if user.is_active:
					auth_login(request, user)
					for uu in user.groups.all():
						if uu.id == 1:
							return HttpResponse(simplejson.dumps({"response":True, 'fronts' : FrontDesk().get_FrontByUser(user), 'more' : '/usuario/'}), mimetype='application/json')
					else:
						return HttpResponse(simplejson.dumps({"response":True, 'fronts' : FrontDesk().get_FrontByUser(user)}), mimetype='application/json')
				else:
					context = {
						'aplicacion' : getAplicacion(),
						'formlogin' : form,
					}
					return HttpResponse(simplejson.dumps({"response":"false",}), mimetype='application/json')
			else:
				context = {
					'aplicacion' : getAplicacion(),
					'formlogin' : form,
				}
				return HttpResponse(simplejson.dumps({"response":"false",}), mimetype='application/json')
		else:
			form = LoginForm()
			context = {
				'aplicacion' : getAplicacion(),
				'formlogin' : form,
			}
		return HttpResponse(simplejson.dumps({"response":"false",}), mimetype='application/json')
	else:
		logout(request)
		form = LoginForm()
		context = {
			'aplicacion' : getAplicacion(request),
			'formlogin' : form,
		}
	return render_to_response('login.html', context, context_instance = RequestContext(request))
	
def login_admin(request):
	if request.method == 'POST':
		form = LoginForm(request.POST)
		if form.is_valid():
			username = form.cleaned_data['username']
			passwd = form.cleaned_data['password']
			user = authenticate(username = username, password = passwd)
			if user is not None:
				if user.is_active:
					auth_login(request, user)
					return HttpResponseRedirect('/usuario/')
				else:
					context = {
						'aplicacion' : getAplicacion(),
						'formlogin' : form,
					}
					return HttpResponse(simplejson.dumps({"response":"false",}), mimetype='application/json')
			else:
				context = {
					'aplicacion' : getAplicacion(),
					'formlogin' : form,
				}
				return HttpResponse(simplejson.dumps({"response":"false",}), mimetype='application/json')
		else:
			form = LoginForm()
			context = {
				'aplicacion' : getAplicacion(),
				'formlogin' : form,
			}
		return HttpResponse(simplejson.dumps({"response":"false",}), mimetype='application/json')
	else:
		logout(request)
		form = LoginForm()
		context = {
			'aplicacion' : getAplicacion(request),
			'formlogin' : form,
		}
	return render_to_response('login_admin.html', context, context_instance = RequestContext(request))

def onlogout(request, parametro = None):
	logout(request)
	return HttpResponseRedirect('/login/')


def index_local(request, local, parametro = None):
	usuario = request.user
	if not (isLogin(usuario)):
		return HttpResponseRedirect("/login/")
	if not getAplicacion(request, local) == False:
		if (okPermission(usuario.groups.all(), 1) or okPermission(usuario.groups.all(), 3)):
			aplicacion = getAplicacion(request, local)
			productos = Producto.objects.all()
			_tmp = []
			for x in Stock.objects.all():
				_tmp.append({
					'id' : str(x.producto.id) + "o" + str(x.id) + "o" + str(x.lote.id) + "o" + str(0),
					'stock_id' : x.id,
					'stock_cantidad' : x.cantidad - x.cantidad_inversa,
					'stock_granel_id' : 0,
					'stock_granel_cantidad' : 0,
					'lote_id' : x.lote.id,
					'lote_nombre' : x.lote.nombre,
					'presentacion' : x.producto.presentacion_c,
					'precio' : x.precio,
					'nombre' : x.producto.producto,
					'nombrec' : x.producto.nombre_comercial,
				})
				
			for x in StockGranel.objects.all():
				_tmp.append({
					'id' : str(x.stock.producto.id) + "o" + str(x.stock.id) + "o" + str(x.stock.lote.id) + "o" + str(x.id),
					'stock_id' : 0,
					'stock_cantidad' : 0,
					'stock_granel_id' : x.id,
					'stock_granel_cantidad' : x.cantidad - x.cantidad_inversa,
					'lote_id' : x.stock.lote.id,
					'lote_nombre' : x.stock.lote.nombre,
					'presentacion' : x.stock.producto.presentacion_u,
					'precio' : x.precio,
					'nombre' : x.stock.producto.producto,
					'nombrec' : x.stock.producto.nombre_comercial,
				})

			variables = {
				'aplicacion' : getAplicacion(request, local),
				'items' : getMenuFirst(usuario.groups.all()),
				'menu' :  getMenuSecond(usuario.groups.all()),
				'itres' : getThirdSecond(usuario.groups.all()),
				'productos' : [],
			}
			return render_to_response('index.html', variables, context_instance = RequestContext(request))
	return HttpResponseRedirect("/usuario/")

def dashboard_local(request, local):
	usuario = request.user
	if not (isLogin(usuario)):
		return HttpResponseRedirect("/login/")
	if not getAplicacion(request, local) == False:
		if (okPermission(usuario.groups.all(), 1) or okPermission(usuario.groups.all(), 3)):
			aplicacion = getAplicacion(request, local)
			IVenta = Venta()
			variables = {
				'aplicacion' : aplicacion,
				'items' : getMenuFirst(usuario.groups.all()),
				'menu' :  getMenuSecond(usuario.groups.all()),
				'itres' : getThirdSecond(usuario.groups.all()),
			}
			return render_to_response('dashboard_local.html', variables, context_instance = RequestContext(request))
	return HttpResponseRedirect("/login/")
	
def dash_admin(request):
	usuario = request.user
	if not (isLogin(usuario)):
		return HttpResponseRedirect("/login/")
	if (okPermission(usuario.groups.all(), 1)):
		variables = {
			'aplicacion' : getAplicacion(request),
			'items' : getMenuFirst(usuario.groups.all()),
			'menu' :  getMenuSecond(usuario.groups.all()),
			'itres' : getThirdSecond(usuario.groups.all()),
		}
		return render_to_response('dashboard_admin.html', variables, context_instance = RequestContext(request))
	return HttpResponseRedirect("/login/")
	
def vender(request):
	if request.method == 'POST': 
		print request.POST
		return HttpResponse(simplejson.dumps({"success":"1",}), mimetype='application/json')
	logear(str(datetime.datetime.now()) + " - profesional_h_change " + " - " + str(usuario) + " Denegación de servicios.")
	return HttpResponse(simplejson.dumps({"success":"0",}), mimetype='application/json')			

