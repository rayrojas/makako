$.fn.dataTableExt.oApi.fnReloadAjax = function ( oSettings, sNewSource, fnCallback, bStandingRedraw ) {
	if ( typeof sNewSource != 'undefined' && sNewSource != null ) {
		oSettings.sAjaxSource = sNewSource;
	}
	this.oApi._fnProcessingDisplay( oSettings, true );
	var that = this;
	var iStart = oSettings._iDisplayStart;
	var aData = [];
	this.oApi._fnServerParams( oSettings, aData );
	oSettings.fnServerData( oSettings.sAjaxSource, aData, function(json) {
		/* Clear the old information from the table */
		that.oApi._fnClearTable( oSettings );
		/* Got the data - add it to the table */
		var aData =  (oSettings.sAjaxDataProp !== "") ?
		that.oApi._fnGetObjectDataFn( oSettings.sAjaxDataProp )( json ) : json;
		for ( var i=0 ; i<aData.length ; i++ ) {
			that.oApi._fnAddData( oSettings, aData[i] );
		}
		oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
		that.fnDraw();
		if ( typeof bStandingRedraw != 'undefined' && bStandingRedraw === true ) {
			oSettings._iDisplayStart = iStart;
			that.fnDraw( false );
		}
		that.oApi._fnProcessingDisplay( oSettings, false );
		/* Callback user function - for event handlers etc */
		if ( typeof fnCallback == 'function' && fnCallback != null ) {
			fnCallback( oSettings );
		}
	}, oSettings );
}


$(document).ready(function(){
	
	var XJsonTable = $("#xData").dataTable({
        "bProcessing": true,
        "sAjaxSource": "/local/" + self['local'] + "/producto/bk/productos/",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 10,
        "aoColumns": [
            { "mDataProp": "id_nat" },
            { "mDataProp": "presentacion_nat" },
            { "mDataProp": "lote_present_stock" },
            { "mDataProp": "nombre_nombrec" },
            { "mDataProp": "precio" },
			{ "mDataProp": "button-add" },
        ],
		"aoColumnDefs": [ 
			{ /*"bVisible": false,*/"sClass": "mko_pro-id column_hidden", "aTargets": [0] },
			{ /*"bVisible": false,*/"sClass": "mko_pro-pre column_hidden", "aTargets": [1] },

			{ /*"bVisible": false,*/"sClass": "mko_pro-nom", "aTargets": [3] },
			{ /*"bVisible": false,*/"sClass": "mko_pro-pri", "aTargets": [4] },
			{ "sWidth": "10%", "aTargets": [4] },
			{ "sWidth": "2%", "aTargets": [5] }
			
		],
		"fnInitComplete": function(oSettings, json) {
	
		},
		"fnDrawCallback": function( oSettings ) {
			$(".mko_add-product").click(function(){
				var product = $(this).parent().parent();
				if (store().filter({id:product.children(".mko_pro-id").text()}).count() == 0) {
					store.insert({
						id : product.children(".mko_pro-id").text(),
						type : product.children(".mko_pro-pre").text(),
						amount : 1,
						name : product.children(".mko_pro-nom").text(),
						namec : product.children(".mko_pro-com").text(),
						price : parseFloat(product.children(".mko_pro-pri").text()).toFixed(2)
					});
					store().show(store());
				} else {
					store().filter({id:product.children(".mko_pro-id").text()}).update({
						amount : (parseInt(store().filter({id:product.children(".mko_pro-id").text()}).select('amount')) + 1)}
					);
					store().show(store());
					$('#'+product.children(".mko_pro-id").text()).css('background',"#69A74E");
					setTimeout(function(){$('#'+product.children(".mko_pro-id").text()).css('background',"none");}, 350);
				}
			});
		}
	});
	
	var table = $("#xData_wrapper");
	var info = table.find('.dataTables_info');
	var paginate = table.find('.dataTables_paginate');
	var filtro = table.find('.dataTables_filter');
	$("#xData").after('<div class="action_bar nomargin"></div>');
	table.find('.action_bar').prepend(info).append(paginate);
	$("#find-products").append(filtro);
	
	var store = new TAFFY();
	var state = 0;
	humane.timeout = 4000;
	humane.clickToClose = true;	
	TAFFY.extend("show",function (db) {
		$("#mko_tbl-products").children("tbody").children("tr").remove();
		$("#mko_tbl-products").children("tbody").append(db.supplant("<tr id='{id}'><td class='mko_pro-id' style='display:none;'>{id}</td><td>{type}</td><td><input class='mko_pro-cant small' type='text' size='1' value='{amount}'></td><td>{name}</td><td>{price}</td><td><a href='#' class='button plain plain-red mko_out-product super-small'><span class='glyph minus'></span></a></td></tr>"));
		$(".mko_pro-cant").keyup(function(e){
			var code = (e.keyCode ? e.keyCode : e.which);
			if ( code == 38 ) {
				$(this).val(parseInt(store().filter({id:$(this).parent().parent().children('.mko_pro-id').text()}).select('amount')) + 1);
			} else if ( code == 40 ){
				$(this).val(parseInt(store().filter({id:$(this).parent().parent().children('.mko_pro-id').text()}).select('amount')) - 1);
			} else if ( code >= 48 && code <= 57 ) {
			} else if ( code == 8 ) {
				if ($(this).val() == ""){
					$(this).val("1");
				}
			} else {
				$(this).val(store().filter({id:$(this).parent().parent().children('.mko_pro-id').text()}).select('amount'));
			}
			if (parseInt($(this).val()) <= 0){
				$(this).val("1");
				return false;
			}
			store().filter({id:$(this).parent().parent().children('.mko_pro-id').text()}).update({amount : parseInt($(this).val())});
			$("#mko_price-total").text(store().subTotal("price","amount"));
			
		});
		$(".mko_pro-cant").focus(function(){
			$(this).select();
		});
		$(".mko_pro-cant").change(function(){
			$(this).css("background","#000");
		});
		$(".mko_out-product").click(function(){
			store().filter({id:$(this).parent().parent().children('.mko_pro-id').text()}).remove();
			store().show(store());
		});
		$("#mko_price-total").text(store().subTotal("price","amount"));
	});
	store.settings({
		onInsert : function(){
			//console.log("insertar");
		},
		onUpdate : function(end, start){
			//console.log("update");
		},
		onRemove : function(){
			//console.log("remove");
		}
	});
	$("#mko_form-coin").data("validator").settings.submitHandler = function() {
		if ( store().count() > 0 ){
			$("#send-topay").attr("disabled",true);
			$("#send-topay").val("Enviando...");
			var variables = {
				csrfmiddlewaretoken : $("#mko_form-coin").find( 'input[name="csrfmiddlewaretoken"]' ).val(),
				productos : store().supplant(",{id}-{amount}"),
				ruc : $("#mko_form-coin").find( 'input[name="ruc"]' ).val()
			};
			$.post("/local/" + self['local'] + "/producto/bk/productos/venta/", variables)
			.success(function(data){
				if ( data.status == "1" ) {
					$("#send-topay").attr("disabled",false);
					$("#send-topay").val("Vender");
					XJsonTable.fnReloadAjax();
				} else {
					if ( data.msg != undefined ) {
						humane.log(data.msg, function(){
							$("#send-topay").attr("disabled",false);
							$("#send-topay").val("Vender");
						});
					} else {
						humane.log("Error. Contacte al Administrador.", function(){
							$("#send-topay").attr("disabled",false);
							$("#send-topay").val("Vender");
						});
					}
				}
				store().remove();
				store().show(store());
			}, "json")
			.error(function() {
				humane.log("Error. Contacte al Administrador.", function(){
					$("#send-topay").attr("disabled",false);
					$("#send-topay").val("Vender");
				});
				store().remove();
				store().show(store());
			});
		}
		return false;
	}
	function getVariables() {
		if ( $("#ruc").val() == "" )
		return {
			csrfmiddlewaretoken : $("#mko_form-coin").find( 'input[name="csrfmiddlewaretoken"]' ).val(),
			productos : store().supplant(",{id}-{amount}"),
		};
	}
});

