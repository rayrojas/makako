jQuery.validator.addMethod("dateITA",
	function(value, element) {
        var check = false;
        var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
        if( re.test(value)){
            var adata = value.split('/');
            var gg = parseInt(adata[0],10);
            var mm = parseInt(adata[1],10);
            var aaaa = parseInt(adata[2],10);
            var xdata = new Date(aaaa,mm-1,gg);
            if ( ( xdata.getFullYear() == aaaa ) 
                   && ( xdata.getMonth () == mm - 1 ) 
                   && ( xdata.getDate() == gg ) )
                check = true;
            else
                check = false;
        } else{
            check = false;}
        return this.optional(element) || check;
    },
    "Ingrese una Fecha Correcta."
);
/*
jQuery.validator.addMethod("dni",
	function(value, element) {
        check = false;
		check = $.post("/paciente/bk/dni/really/",
			{csrfmiddlewaretoken : $(element).parent().parent().find( 'input[name="csrfmiddlewaretoken"]' ).val(), dni : $(element).val()},
			function(data) {
				if ( data.success == "0" ){
					console.log("disponible");
					check = true;
					return true;
				} else {
					console.log("indisponible");
					check = false;
					return false;
				}
				}, "json");
		console.log(check);
        return this.optional(element) || check;
    },
    "DNI ocupado por otro Paciente."
);*/

$('document').ready(function() {
  
  /******************
    Tablet rotation
  ******************/
  
  var isiPad = navigator.userAgent.match(/iPad/i) != null;
  
  if(isiPad) {
    $('body').prepend('<div id="rotatedevice"><h1>Please rotate your device 90 degrees.</div>');
  }
  
  /********
    Login
  ********/
  
  $('#login_entry > a').click(function() {    
    $(this).fadeOut(200, function() {
      $('#login_form').fadeIn();
    });

    return false;
  });
  
  /********************
    Modal preparation
  ********************/
  
  $('body').prepend('<div id="overlay"><div id="modalcontainer"></div></div>');
  
  /*******
    PJAX
  *******/
/*  
  $('nav#primary a').click(function() {
    window.location = $(this).attr("href");
    return false;
  });
  
  $('nav#secondary a').pjax({
    container: '#main',
    success: function(data) {
      init();
    }
  });
 */ 
  /****************
    Notifications
  ****************/
  
  $('#notifications').prepend('<a href="#">Show all notifications</a>');
  
  $('#notifications > a').click(function() {
    var container = $('#notifications');
    var height = $('#notifications ul').height() + 24;
    
    if(container.hasClass('expanded')) {
      container.animate({'height': 42}, 200);
      container.removeClass('expanded');
      $(this).html('show all notifications');
    } else {
      container.animate({'height': height}, 200);
      container.addClass('expanded');
      $(this).html('hide notifications');
    }
    
    return false;
  });
  
  function init() {
    
    /*************
      Datepicker
    *************/
    
    $('.datepicker').datepicker({dateFormat: 'dd/mm/yy'});
    //$('.datebirth').datepicker({yearRange: "-20:+0",dateFormat: 'dd/mm/yy', changeYear: true});
    //$('.datebirth').datepicker("setDate", new Date(1989,10,30) );

        
    /*****************
      Wysiwym-editor
    *****************/
   
   /* $('.wysiwym').wymeditor({
      logoHtml: '',
      toolsItems: [
        {'name': 'Bold', 'title': 'Strong', 'css': 'wym_tools_strong'}, 
        {'name': 'Italic', 'title': 'Emphasis', 'css': 'wym_tools_emphasis'},
        {'name': 'InsertOrderedList', 'title': 'Ordered_List', 'css': 'wym_tools_ordered_list'},
        {'name': 'InsertUnorderedList', 'title': 'Unordered_List', 'css': 'wym_tools_unordered_list'},
        {'name': 'Indent', 'title': 'Indent', 'css': 'wym_tools_indent'},
        {'name': 'Outdent', 'title': 'Outdent', 'css': 'wym_tools_outdent'},
        {'name': 'CreateLink', 'title': 'Link', 'css': 'wym_tools_link'},
        {'name': 'Paste', 'title': 'Paste_From_Word', 'css': 'wym_tools_paste'},
        {'name': 'Undo', 'title': 'Undo', 'css': 'wym_tools_undo'},
        {'name': 'Redo', 'title': 'Redo', 'css': 'wym_tools_redo'},
      ],
      containersItems: [
        {'name': 'P', 'title': 'Paragraph', 'css': 'wym_containers_p'},
        {'name': 'H4', 'title': 'Heading_4', 'css': 'wym_containers_h4'}
      ]
    });
    */
    /************
      Code view
    ************/
  
    $('code').each(function() {
      var elem = $(this);
      var lang = elem.attr("class");
    
      elem.sourcerer(lang);
    });
  
    /*************
      Datatables
    *************/
    $('.datatable').dataTable({
      "sPaginationType": "full_numbers",
      "bStateSave": true
    });
  
    $('.dataTables_wrapper').each(function() {
      var table = $(this);
      var info = table.find('.dataTables_info');
      var paginate = table.find('.dataTables_paginate');
    
      table.find('.datatable').after('<div class="action_bar nomargin"></div>');
      table.find('.action_bar').prepend(info).append(paginate);
    });
    /************************
      Combined input fields
    ************************/
    
    $('div.combined p:last-child').addClass('last-child');
  
    /**********
      Sliders
    **********/
  
    $(".slider").each(function() {
      var options = $(this).metadata();
      $(this).slider(options, {
        animate: true
      });
    });
  
    $(".slider-vertical").each(function() {
      var options = $(this).metadata();
      $(this).slider(options, {
        animate: true
      });
    });
    
    /*******
      scape
    *******/
	$(document.documentElement).keypress(function (e) {
		var code = (e.keyCode ? e.keyCode : e.which);
		if ( code == 27 ) {
			//do some whit space
			$('#modalcontainer > div').hide();
			$('#overlay').hide();
			return false;
		}
	});
    
    /*******
      Tags
    *******/
    
    $('.taginput').tagsInput({
      'width':'auto'
    });
  
    /****************
      Progress bars
    ****************/
  
    $(".progressbar").each(function() {
      var options = $(this).metadata();
      $(this).progressbar(options);
    });
  
    /**********************
      Modal functionality
    **********************/
  
    $('a.modal').each(function() {
      var link = $(this);
      var id = link.attr('href');
      var target = $(id);
      if($("#modalcontainer " + id).length == 0) {
        $("#modalcontainer").append(target);
      }
      
      $("#main " + id).remove();
    
      link.click(function() {
        $('#modalcontainer > div').hide();
        target.show();
        $('#overlay').show();
      
        return false;
      });
    });
  
    $('.close').click(function() {
      $('#modalcontainer > div').hide();
      $('#overlay').hide();
    
      return false;
    });
    
    /***********************
      Secondary navigation
    ***********************/
    
    $('nav#secondary > ul > li > a').click(function() {
      $('nav#secondary li').removeClass('active');
      $(this).parent().addClass('active');
    });
  
    /********************
      Pretty checkboxes
    ********************/
  
    $('input[type=checkbox], input[type=radio]').each(function() {
      if($(this).siblings('label').length > 0) {
        $(this).prettyCheckboxes();
      }
    });
  
    /**********************
      Pretty select boxes
    **********************/
  
    $('select:not(#whitout-fx, .whitout-fx)').chosen();
  

    
    /***********
      Tooltips
    ***********/
    
    $('.tooltip').tipsy({gravity: 'n'});
    
    /***********
      Tooltips for documentation
    ***********/
    if ( typeof(IsDoc) != "undefined" )
		if (IsDoc)
			$('.tooldoc').tipsy({gravity: 'n'});
  
    /******************
      Form Validation
    ******************/
  
    $('form:not(#whitout-validate, .whitout-validate)').validate({
      wrapper: 'span class="error"',
      meta: 'validate',
      highlight: function(element, errorClass, validClass) {
        if (element.type === 'radio') {
          this.findByName(element.name).addClass(errorClass).removeClass(validClass);
        } else {
          $(element).addClass(errorClass).removeClass(validClass);
        }
      
        // Show icon in parent element
        var error = $(element).parent().find('span.error');
      
        error.siblings('.icon').hide(0, function() {
          error.show();
        });
      },
      unhighlight: function(element, errorClass, validClass) {
        if (element.type === 'radio') {
          this.findByName(element.name).removeClass(errorClass).addClass(validClass);
        } else {
          $(element).removeClass(errorClass).addClass(validClass);
        }
      
        // Hide icon in parent element
        $(element).parent().find('span.error').hide(0, function() {
          $(element).parent().find('span.valid').fadeIn(200);
        });
      }/*,
      rules : {
			nameField: {noEqual: "Your Name"}
		}*/
    });
    
    /******************
      Form Validation Ajax
    ******************/
  
    $('.F-Ajax').validate({
	 submitHandler: function() {
		return false;
	 },
      wrapper: 'span class="error"',
      meta: 'validate',
      highlight: function(element, errorClass, validClass) {
        if (element.type === 'radio') {
          this.findByName(element.name).addClass(errorClass).removeClass(validClass);
        } else {
          $(element).addClass(errorClass).removeClass(validClass);
        }
      
        // Show icon in parent element
        var error = $(element).parent().find('span.error');
      
        error.siblings('.icon').hide(0, function() {
          error.show();
        });
      },
      unhighlight: function(element, errorClass, validClass) {
        if (element.type === 'radio') {
          this.findByName(element.name).removeClass(errorClass).addClass(validClass);
        } else {
          $(element).removeClass(errorClass).addClass(validClass);
        }
      
        // Hide icon in parent element
        $(element).parent().find('span.error').hide(0, function() {
          $(element).parent().find('span.valid').fadeIn(200);
        });
      }/*,
      rules : {
			nameField: {noEqual: "Your Name"}
		}*/
    });
    
    /******************
      Form Validation Ajax
    ******************/
  
    $('.F-Search').validate({
	 submitHandler: function() {
		return false;
	 },
      wrapper: 'span class="error"',
      meta: 'validate',
      highlight: function(element, errorClass, validClass) {
        if (element.type === 'radio') {
          this.findByName(element.name).addClass(errorClass).removeClass(validClass);
        } else {
          $(element).addClass(errorClass).removeClass(validClass);
        }
      
        // Show icon in parent element
        var error = $(element).parent().find('span.error');
      
        error.siblings('.icon').hide(0, function() {
          error.show();
        });
      },
      unhighlight: function(element, errorClass, validClass) {
        if (element.type === 'radio') {
          this.findByName(element.name).removeClass(errorClass).addClass(validClass);
        } else {
          $(element).removeClass(errorClass).addClass(validClass);
        }
      
        // Hide icon in parent element
        $(element).parent().find('span.error').hide(0, function() {
          $(element).parent().find('span.valid').fadeIn(200);
        });
      }/*,
      rules : {
			nameField: {noEqual: "Your Name"}
		}*/
    });
    

    // Add valid icons to validatable fields
    $('form p > *').each(function() {
      var element = $(this);
      
      if(element.metadata().validate) {        
        element.parent().append('<span class="icon tick valid"></span>');
      }
    });
  }
  
  init();

	/*$("#search-fast").find("form").data("validator").settings.submitHandler = function(){
		$("#search-fast").find("input[type='submit']").attr("disabled",true);
		$("#search-fast").find("input[type='submit']").attr("value","Buscando");
		$.get( "/paciente/bk/search/" + $("#search-fast").find("input[name='txtsearch']").val() + "/",
			function( data ) {
				if ( data.success == "0" ) {
					$("#search-fast").find("ul").html("");
					$("#search-fast").find("ul").prepend("<li>No se encontró.</li>");
				} else {
					var xrx = data.length;
					var xulx = $("#search-fast").find("table");
					xulx.html("");
					var strHtml = "";
					for (var x=0; x < xrx; x++ ){
						strHtml += "<tr><td>" + data[x].dni +"</td><td>" + data[x].appaterno + " " + data[x].apmaterno + " " + data[x].nombre +"</td><td><a class='button plain plain-green' href='/paciente/datos/"
									+ data[x].dni + "/'><span class='glyph listicon'></span></a><a class='button plain plain-bear' href='/paciente/paquete/"
									+ data[x].dni + "/'><span class='glyph dropbox'></span></a><a class='button plain plain-green' href='/paciente/mi-calendario/"
									+ data[x].dni + "/'><span class='glyph calendar'></span></a><a class='button plain plain-blue' href='#'><span class='glyph file'></span></a>"
									+ "</td></tr>";
					}
					xulx.html(strHtml);
					xulx.show();
				}
				$("#search-fast").find("input[type='submit']").attr("disabled",false);
				$("#search-fast").find("input[type='submit']").attr("value","Buscar");
			}, "json"
		)
		.error(function() {
			$("#search-fast").find("input[type='submit']").attr("disabled",false);
			$("#search-fast").find("input[type='submit']").attr("value","Buscar");
		 });
		return false;
	};*/
	/*if ($("#search-fast").find("input[name='txtsearch']").length){
		$("#search-fast").find("input[name='txtsearch']").on("keyup", function(){
			$("#search-fast").find("form").trigger("submit");
		});
		$("#search-fast").find("input[name='txtsearch']").on("focus", function(){
			$(this).val("");
		});
	}*/
	
});

/*************************
  Notification function!
*************************/

function notification(message, error, icon, image) {
  if(icon == null) {
    icon = 'tick2';
  }
  
  if(image) {
    image = 'icon16';
  } else {
    image = 'glyph';
  }
  
  var now = new Date();
  var hours = now.getHours();
  var minutes = now.getMinutes();
    
  if (hours < 10) {
    hours = "0" + hours;
  }
  
  if (minutes < 10) {
    minutes = "0" + minutes;
  }
  
  var time = hours + ':' + minutes;
  
  if(error) {
    $('#notifications ul').append('<li class="error"><span class="' + image + ' cross"></span> ' + message + ' <span class="time">' + time + '</span></li>');
  } else {
    $('#notifications ul').append('<li><span class="' + image + ' ' + icon + '"></span> ' + message + ' <span class="time">' + time + '</span></li>');
  }
  
  $('#notifications ul li:last-child').hide();
  $('#notifications ul li:last-child').slideDown(200);
}
