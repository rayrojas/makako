$(document).on("ready", function(){
	$("#form-login").on("submit", function() {
		$("#sendform").attr("disabled", true);
		$("#sendform").val("Enviando...");
		variables = {
			csrfmiddlewaretoken : $("#form-login").find( 'input[name="csrfmiddlewaretoken"]' ).val(),
			username : $("#id_username").val(),
			password : $("#id_password").val()
		};
		$.post("/login/", variables)
		.success(function(data){
			if ( data.response == true ) {
				$("#sendform").attr("disabled", false);
				$("#sendform").hide();
				$("#sendform").next().hide();
				$("#id_username").parent().hide();
				$("#id_password").parent().hide();
				select = "<select id='select-front'>";
				for ( var x = 0; x < data.fronts.length; x++ ) {
					select += "<option value='" + data.fronts[x]['id'] + "'>" + data.fronts[x]['nombre'] + "</option>";
				}
				select += "</select>";
				$("#form-login").prepend("<p class='w-100'>" + select + "</p>");
				$("#to-front").show();
				$('select:not(#whitout-fx, .whitout-fx)').chosen();
				if ( data.more != undefined ) {
					$("#to-front").after("<a href='" + data.more + "' class='button blue'>Administrador</a>");
				}
			} else {
				console.log("error");
				$("#sendform").attr("disabled", false);
				$("#sendform").val("Ingresar");
			}
		})
		.error(function(){
			$("#sendform").attr("disabled", false);
			$("#sendform").val("Ingresar");
		})
		return false;
	});
	$("#to-front").click(function(){
		$(this).attr("href","/local/" + $("#select-front").val() + "/");
	});	
});
