# -*- coding: utf-8 -*-
from django.contrib import admin
from farmacia.models import Producto
from farmacia.models import Usuariox
from farmacia.models import Laboratorio
from farmacia.models import Malestar
from farmacia.models import Componente
from farmacia.models import Presentacion
from farmacia.models import Producto
from farmacia.models import Lote
from farmacia.models import Detalle
from farmacia.models import Venta
from farmacia.models import Stock
from farmacia.models import StockGranel
from farmacia.models import Proveedor
from farmacia.models import FrontDesk
from farmacia.models import StockForFront
from farmacia.models import StockGranelForFront
from farmacia.models import Aplicacion
from farmacia.models import MenuFirstLevel
from farmacia.models import MenuSecondLevel
from farmacia.models import MenuThirdLevel
from farmacia.models import StockUnidadTemp
from farmacia.models import StockUnidad
from farmacia.models import StockGranelOfTemp

admin.site.register(Producto)
admin.site.register(Usuariox)
admin.site.register(Laboratorio)
admin.site.register(Componente)
admin.site.register(Presentacion)
admin.site.register(Malestar)
admin.site.register(Lote)
admin.site.register(Venta)
admin.site.register(Detalle)
admin.site.register(Stock)
admin.site.register(StockGranel)
admin.site.register(Proveedor)
admin.site.register(StockForFront)
admin.site.register(FrontDesk)
admin.site.register(StockGranelForFront)
admin.site.register(Aplicacion)
admin.site.register(MenuFirstLevel)
admin.site.register(MenuSecondLevel)
admin.site.register(MenuThirdLevel)
admin.site.register(StockUnidadTemp)
admin.site.register(StockUnidad)
admin.site.register(StockGranelOfTemp)
