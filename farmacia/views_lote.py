# encoding:utf-8
from django.shortcuts import render_to_response
from django.utils import simplejson
from django.utils.encoding import smart_str, smart_unicode
from django.template import RequestContext
from django.contrib import messages
from django.contrib.sessions.backends.db import SessionStore
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth import login as auth_login
from django.contrib.auth.models import User, Group
from django.http import HttpResponseRedirect, HttpResponse
from views import getAplicacion, getMenuFirst, getMenuSecond, isLogin, okPermission
from farmacia.models import Laboratorio
from farmacia.forms import LaboratorioForm
from farmacia.models import Lote
from farmacia.forms import LoteForm
from farmacia.models import Componente
from farmacia.forms import ComponenteForm
from farmacia.models import Malestar
from farmacia.forms import MalestarForm
from farmacia.models import Producto
from farmacia.forms import ProductoForm
from sislog import logear
import datetime
import time
import json
import traceback

class ObjectWork:
	_Object = []
	_ObjectForm = []
	_Title = ""
	_Titles = ""
	_Path = ""
	_Html = ""
#maestro Lotes
def RLote(request, codigo = ""):
	usuario = request.user
	if not (isLogin(usuario)) and (Usuariox.objects.all()):
		logear(str(datetime.datetime.now()) + " - RLote" + " - " + str(usuario) + " No inicio sesión.")
		return HttpResponseRedirect("/login/")
	if (okPermission(usuario.groups.all(), 1)):
		# Variables
		_Object = ObjectWork()
		_Object._Object = Lote
		_Object._ObjectForm = LoteForm
		_Object._Title = "Lote"
		_Object._Titles = "Lotes"
		_Object._Path = "/administrator/farmacia/lote/"
		_Object._Html = "RForm.html"
		Generals = {
			'aplicacion' : getAplicacion(request = request),								# Trae el objeto Aplicación, consiste en datos generales del sistema
			'items' : getMenuFirst(usuario.groups.all()),				# Trae objetos que representan opciones de un menu principal, relativo a un usuario
			'menu' :  getMenuSecond(usuario.groups.all()),				# Trae objetos que representan opciones de un menu secundario, relativo a un usuario
			'titulo' : 'Formulario de ' + _Object._Titles,					# Titulo del formulario de registro
			'titulo_segundo' : "Lista de " + _Object._Titles,				# Titulo del listo de objetos
			'action' : _Object._Path,
			'objetos' : _Object._Object.objects.all(),	# Objetos que serán listados
			'suject' : [],												# Objeto que será editado
			'mensaje' : "",												# Mensaje de éxito o fracazo
			'Url' : request.get_full_path(),							# Dirección URL importante para el menu secundario
		}	
		if request.method == 'POST':
			# Todas las operaciones que sucedan a continuación se realizarán solo cuando se haga POST
			if codigo:
			# Si tiene código, significa que vamos a editar un registro
				try:
					Generals['suject'] = _Object._Object.objects.get(id = codigo)	# El objeto a editar a guardado en suject, que antes estaba vacio
					form = _Object._ObjectForm(request.POST, instance = Generals['suject'])
					print form
					Generals['mensaje'] = "Se modificó satisfactoriamente. " + smart_str(Generals['suject'])	#La función smart_str exita errores de tildes
				except Exception, e:
					messages.error(request, _Object._Title + " a editar no ha sido encontrada.")
					error = traceback.extract_stack()[-1]
					logear(str(datetime.datetime.now()) + " - " + ", ".join([type(e).__name__, str(error[0]), str(error[2]), str(error[1])]))
					return HttpResponseRedirect(_Object._Path)
			else:
				# Nos indica que se trata de un registro nuevo
				form = _Object._ObjectForm(request.POST)
				print form
				Generals['mensaje'] = "Se registró satisfactoriamente."
			if form.is_valid():
				# Validamos los datos a guardar
				form.save()
				messages.success(request, Generals['mensaje'])
				return HttpResponseRedirect(_Object._Path)
			else:
				# Si los datos no son validos
				if codigo:
					# Si esque se trataba de un registro existente
					messages.error(request, "No se modificaron los datos.")
					codigo += "/"
				else:
					# Si esque se trataba de un nuevo registro
					messages.error(request, "No se registraron los datos.")
				# Preparamos las variables más los errores que tiene el form
				context = {
					'aplicacion' : Generals['aplicacion'],
					'items' : Generals['items'],
					'menu' :  Generals['menu'],
					'titulo' : Generals['titulo'],
					'titulo_segundo' : Generals['titulo_segundo'],	
					'action' : Generals['action'],
					'objetos' : Generals['objetos'],
					'Url' : Generals['Url'],
					'form' : form,
					'codigo' : codigo,
					'Url' : Generals['Url'],
				}
		else:
			# Cuando sea la primera vez que ingresa a esta vista
			if codigo:
				# Si posee codigo
				try:
					Generals['suject'] = _Object._Object.objects.get(id = codigo)
					form = _Object._ObjectForm(instance = Generals['suject'], initial = { 'usuariox':usuario.id })	# Pasando datos a un formulario
					codigo = str(codigo) + "/"	# Esta variable es para el action del formulario
					messages.info(request, ("Editando datos de " + smart_str(Generals['suject'])))
				except Exception, e:
					messages.error(request, _Object._Title + " a editar no ha sido encontrada.")
					error = traceback.extract_stack()[-1]
					logear(str(datetime.datetime.now()) + " - " + ", ".join([type(e).__name__, str(error[0]), str(error[2]), str(error[1])]))
					return HttpResponseRedirect(_Object._Path)
			else:
				# Mostrar un formulario vacio
				form = _Object._ObjectForm(initial = { 'usuariox':usuario.id })
			context = {
					'aplicacion' : Generals['aplicacion'],			# Datos de la aplicación
					'items' : Generals['items'],					# Opciones del menú primario
					'menu' :  Generals['menu'],						# Opciones del menú secundario
					'titulo' : Generals['titulo'],					# Titulo del formulario
					'titulo_segundo' : Generals['titulo_segundo'],	# Titulo de la lista de Especialidades
					'action' : Generals['action'],					# Action del formulario
					'objetos' : Generals['objetos'],				# Especialidades
					'Url' : Generals['Url'],						# Indicador del menú secundario
					'codigo' : codigo,								# Código del objeto editado o caso contrario ""
					'form' : form,									# Formulario Vacio o con datos de una especialidad
			}
		return render_to_response(_Object._Html, context, context_instance = RequestContext(request))
	logear(str(datetime.datetime.now()) + " - especialidad_registro" + " - " + str("usuario") + " Denegación de servicios.")
	return HttpResponseRedirect('/')	
	
