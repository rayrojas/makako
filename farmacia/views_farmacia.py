# encoding:utf-8
from django.shortcuts import render_to_response
from django.utils import simplejson
from django.utils.encoding import smart_str, smart_unicode
from django.template import RequestContext
from django.contrib import messages
from django.contrib.sessions.backends.db import SessionStore
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth import login as auth_login
from django.contrib.auth.models import User, Group
from django.http import HttpResponseRedirect, HttpResponse
from views import getAplicacion, getMenuFirst, getMenuSecond, isLogin, okPermission
from farmacia.models import Laboratorio
from farmacia.forms import LaboratorioForm
from farmacia.models import Lote
from farmacia.forms import LoteForm
from farmacia.models import Componente
from farmacia.forms import ComponenteForm
from farmacia.models import Malestar
from farmacia.forms import MalestarForm
from farmacia.models import Producto
from farmacia.forms import ProductoForm
from farmacia.models import Stock
from farmacia.models import StockGranel
from farmacia.models import Farmacia
from farmacia.forms import FarmaciaForm
from sislog import logear
import datetime
import time
import json
import traceback

def farmacia_configuracion(request):
	usuario = request.user
	Generals = {
		'aplicacion' : getAplicacion(),								# Trae el objeto Aplicación, consiste en datos generales del sistema
		'items' : getMenuFirst(usuario.groups.all()),				# Trae objetos que representan opciones de un menu principal, relativo a un usuario
		'menu' :  getMenuSecond(usuario.groups.all()),				# Trae objetos que representan opciones de un menu secundario, relativo a un usuario
		'titulo' : 'Configuración de la Farmacia',					# Titulo del formulario de registro
		'titulo_segundo' : "Configuración actual",				# Titulo del listo de objetos
		'mensaje' : "",												# Mensaje de éxito o fracazo
		'Url' : request.get_full_path(),							# Dirección URL importante para el menu secundario
	}

	context = {
		'aplicacion' : Generals['aplicacion'],								# Trae el objeto Aplicación, consiste en datos generales del sistema
		'items' : Generals['items'],				# Trae objetos que representan opciones de un menu principal, relativo a un usuario
		'menu' :  Generals['menu'],				# Trae objetos que representan opciones de un menu secundario, relativo a un usuario
		'titulo' : Generals['titulo'],					# Titulo del formulario de registro
		'titulo_segundo' : Generals['titulo_segundo'],				# Titulo del listo de objetos
		'mensaje' : "",												# Mensaje de éxito o fracazo
		'Url' : request.get_full_path(),							# Dirección URL importante para el menu secundario
		'form' : FarmaciaForm(),
	}

	
	return render_to_response('configuracion.html', context, context_instance = RequestContext(request))
	logear(str(datetime.datetime.now()) + " - farmacia_configuracion" + " - " + str("usuario") + " Denegación de servicios.")
	return HttpResponseRedirect('/')
