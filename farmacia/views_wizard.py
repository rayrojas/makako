# encoding:utf-8
from django.shortcuts import render_to_response
from django.utils import simplejson
from django.utils.encoding import smart_str, smart_unicode
from django.template import RequestContext
from django.contrib import messages
from django.contrib.sessions.backends.db import SessionStore
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth import login as auth_login
from django.contrib.auth.models import User, Group
from django.http import HttpResponseRedirect, HttpResponse
from views import getAplicacion, getMenuFirst, getMenuSecond, isLogin, okPermission
from farmacia.models import Laboratorio
from farmacia.forms import LaboratorioForm
from farmacia.models import Lote
from farmacia.forms import LoteForm
from farmacia.models import Componente
from farmacia.forms import ComponenteForm
from farmacia.models import Malestar
from farmacia.forms import MalestarForm
from farmacia.models import Producto
from farmacia.forms import ProductoForm
import datetime
import time
import json
import traceback

def RWizard(request, parametro = ""):
	usuario = request.user
	if not (isLogin(usuario)):
		return HttpResponseRedirect("/login/")	
	if (okPermission(usuario.groups.all(), 1)):
		Generals = {
			'aplicacion' : getAplicacion(),
			'items' : getMenuFirst(usuario.groups.all()),
			'menu' : getMenuSecond(usuario.groups.all()),
			'Url' : request.get_full_path(),
		}		
		if parametro == "lotes":
			dinamic = {
				'formulario' : LoteForm(),
				'titulo' : 'Registro de Lotes',
				'action' : '/farmacia/registro-por-pasos/' + parametro + "/",
			}
		elif parametro == "productos":
			dinamic = {
				'formulario' : ProductoForm(),
				'titulo' : 'Registro de Productos',
				'action' : '/farmacia/registro-por-pasos/' + parametro + "/",
			}
		elif parametro == "compuestos":
			dinamic = {
				'formulario' : ComponenteForm(),
				'titulo' : 'Registro de Componentes',
				'action' : '/farmacia/registro-por-pasos/' + parametro + "/",
			}

		context = {
			'aplicacion' : Generals['aplicacion'],
			'items' : Generals['items'],
			'menu' :  Generals['menu'],
			'Url' : Generals['Url'],
			'dinamic' : dinamic,
		}
	return render_to_response('wizard.html', context, context_instance = RequestContext(request))
