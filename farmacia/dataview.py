#encoding: UTF-8

from farmacia.models import Venta, Detalle, Producto, Stock, StockGranel
from farmacia.forms import VentaForm, DetalleForm, StockForm, StockGranelForm
from views import getAplicacion, getMenuFirst, getMenuSecond, isLogin, okPermission
#Data para el archivo vista contabilidad
def getDataIngreso(r):
	return {
				'Clase' : Venta,
				'Formulario' : VentaForm,
				'Ajax' : True,
				'ToForceReturn' :  {"success":"0",},
				'ErrorForm' : {
					'aplicacion' : getAplicacion(),
					'items' : getMenuFirst(r.user.groups.all()),
					'menu' :  getMenuSecond(r.user.groups.all()),
					'action' : '/form/',
					'Url' : r.get_full_path(),
					'success' : False,
				},
				'Html' : 'Pagos.html',
				'MsgErrorForm' : 'No se pudo registrar el pago',
			}


def getDataDetalle(r):
	return {
				'Clase' : Detalle,
				'Unidad' : Producto,
				'Formulario' : DetalleForm,
				'Ajax' : False,
				'ToForceReturn' :  '/',				
			}

def getDataStock(r):
	return {
				'Clase' : Stock,
				'Formulario' : StockForm,
				'Ajax' : False,
				'ToForceReturn' :  '/',				
			}

def getDataStockGranel(r):
	return {
				'Clase' : StockGranel,
				'Formulario' : StockGranelForm,
				'Ajax' : False,
				'ToForceReturn' :  '/',				
			}
