# encoding:utf-8
from django.shortcuts import render_to_response
from django.utils import simplejson
from django.utils.encoding import smart_str, smart_unicode
from django.template import RequestContext
from django.contrib import messages
from django.contrib.sessions.backends.db import SessionStore
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth import login as auth_login
from django.contrib.auth.models import User, Group
from django.http import HttpResponseRedirect, HttpResponse
from views import getAplicacion, getMenuFirst, getMenuSecond, isLogin, okPermission
from farmacia.models import Laboratorio
from farmacia.forms import LaboratorioForm
from farmacia.models import Lote
from farmacia.forms import LoteForm
from farmacia.models import Componente
from farmacia.forms import ComponenteForm
from farmacia.models import Malestar
from farmacia.forms import MalestarForm
from farmacia.models import Producto
from farmacia.forms import ProductoForm
from farmacia.models import Stock
from farmacia.models import StockGranelOfTemp
from farmacia.models import StockGranel
from farmacia.forms import DetalleForm
from farmacia.forms import VentaForm
from farmacia.models import FrontDesk
from farmacia.models import StockForFront
from farmacia.models import StockGranelForFront
from sislog import logear
import datetime
import time
import json
import traceback

class ObjectWork:
	_Object = []
	_ObjectForm = []
	_Title = ""
	_Titles = ""
	_Path = ""
	_Html = ""
	
#maestro malestar
def RProducto(request, codigo = ""):
	usuario = request.user
	if not (isLogin(usuario)) and (Usuariox.objects.all()):
		logear(str(datetime.datetime.now()) + " - RProducto" + " - " + str(usuario) + " No inicio sesión.")
		return HttpResponseRedirect("/login/")
	if (okPermission(usuario.groups.all(), 1)):
		# Variables
		_Object = ObjectWork()
		_Object._Object = Producto
		_Object._ObjectForm = ProductoForm
		_Object._Title = "Producto"
		_Object._Titles = "Productos"
		_Object._Path = "/administrator/farmacia/producto/"
		_Object._Html = "RForm.html"
		Generals = {
			'aplicacion' : getAplicacion(request = request),								# Trae el objeto Aplicación, consiste en datos generales del sistema
			'items' : getMenuFirst(usuario.groups.all()),				# Trae objetos que representan opciones de un menu principal, relativo a un usuario
			'menu' :  getMenuSecond(usuario.groups.all()),				# Trae objetos que representan opciones de un menu secundario, relativo a un usuario
			'titulo' : 'Formulario de ' + _Object._Titles,					# Titulo del formulario de registro
			'titulo_segundo' : "Lista de " + _Object._Titles,				# Titulo del listo de objetos
			'action' : _Object._Path,
			'objetos' : _Object._Object.objects.all(),	# Objetos que serán listados
			'suject' : [],												# Objeto que será editado
			'mensaje' : "",												# Mensaje de éxito o fracazo
			'Url' : request.get_full_path(),							# Dirección URL importante para el menu secundario
		}	
		if request.method == 'POST':
			# Todas las operaciones que sucedan a continuación se realizarán solo cuando se haga POST
			if codigo:
			# Si tiene código, significa que vamos a editar un registro
				try:
					Generals['suject'] = _Object._Object.objects.get(id = codigo)	# El objeto a editar a guardado en suject, que antes estaba vacio
					form = _Object._ObjectForm(request.POST, instance = Generals['suject'])
					Generals['mensaje'] = "Se modificó satisfactoriamente. " + smart_str(Generals['suject'])	#La función smart_str exita errores de tildes
				except Exception, e:
					messages.error(request, _Object._Title + " a editar no ha sido encontrada.")
					error = traceback.extract_stack()[-1]
					logear(str(datetime.datetime.now()) + " - " + ", ".join([type(e).__name__, str(error[0]), str(error[2]), str(error[1])]))
					return HttpResponseRedirect(_Object._Path)
			else:
				# Nos indica que se trata de un registro nuevo
				form = _Object._ObjectForm(request.POST)
				Generals['mensaje'] = "Se registró satisfactoriamente."
			if form.is_valid():
				# Validamos los datos a guardar
				form.save()
				messages.success(request, Generals['mensaje'])
				return HttpResponseRedirect(_Object._Path)
			else:
				# Si los datos no son validos
				if codigo:
					# Si esque se trataba de un registro existente
					messages.error(request, "No se modificaron los datos.")
					codigo += "/"
				else:
					# Si esque se trataba de un nuevo registro
					messages.error(request, "No se registraron los datos.")
				# Preparamos las variables más los errores que tiene el form
				context = {
					'aplicacion' : Generals['aplicacion'],
					'items' : Generals['items'],
					'menu' :  Generals['menu'],
					'titulo' : Generals['titulo'],
					'titulo_segundo' : Generals['titulo_segundo'],	
					'action' : Generals['action'],
					'objetos' : Generals['objetos'],
					'Url' : Generals['Url'],
					'form' : form,
					'codigo' : codigo,
					'Url' : Generals['Url'],
				}
		else:
			# Cuando sea la primera vez que ingresa a esta vista
			if codigo:
				# Si posee codigo
				try:
					Generals['suject'] = _Object._Object.objects.get(id = codigo)
					form = _Object._ObjectForm(instance = Generals['suject'], initial = { 'usuariox':usuario.id })	# Pasando datos a un formulario
					codigo = str(codigo) + "/"	# Esta variable es para el action del formulario
					messages.info(request, ("Editando datos de " + smart_str(Generals['suject'])))
				except Exception, e:
					messages.error(request, _Object._Title + " a editar no ha sido encontrada.")
					error = traceback.extract_stack()[-1]
					logear(str(datetime.datetime.now()) + " - " + ", ".join([type(e).__name__, str(error[0]), str(error[2]), str(error[1])]))
					return HttpResponseRedirect(_Object._Path)
			else:
				# Mostrar un formulario vacio
				form = _Object._ObjectForm(initial = { 'usuariox' : usuario.id })
			context = {
					'aplicacion' : Generals['aplicacion'],			# Datos de la aplicación
					'items' : Generals['items'],					# Opciones del menú primario
					'menu' :  Generals['menu'],						# Opciones del menú secundario
					'titulo' : Generals['titulo'],					# Titulo del formulario
					'titulo_segundo' : Generals['titulo_segundo'],	# Titulo de la lista de Especialidades
					'action' : Generals['action'],					# Action del formulario
					'objetos' : Generals['objetos'],				# Especialidades
					'Url' : Generals['Url'],						# Indicador del menú secundario
					'codigo' : codigo,								# Código del objeto editado o caso contrario ""
					'form' : form,									# Formulario Vacio o con datos de una especialidad
			}
		return render_to_response(_Object._Html, context, context_instance = RequestContext(request))
	logear(str(datetime.datetime.now()) + " - especialidad_registro" + " - " + str("usuario") + " Denegación de servicios.")
	return HttpResponseRedirect('/')

def bk_productos_data(request, local):
	usuario = request.user
	if not (isLogin(usuario)):
		return HttpResponse(simplejson.dumps({"status" : "0", "msg" : "Inicie sesión."}), mimetype='application/json')
	print usuario.groups.all()
	if (okPermission(usuario.groups.all(), 1) or okPermission(usuario.groups.all(), 2) or okPermission(usuario.groups.all(), 3)):
		try:
			LocalFront = FrontDesk.objects.get( id = local )
		except:
			error = traceback.extract_stack()[-1]
			logear(str(datetime.datetime.now()) + " - " + ", ".join([type(e).__name__, str(error[0]), str(error[2]), str(error[1])]))
			return HttpResponse(simplejson.dumps({"status" : "0", "msg" : "Error, contacte al Administrador."}), mimetype='application/json')
		_tmp = []
		
		for x in StockForFront.objects.filter(frontdesk = LocalFront):
			
			_tmp.append({
				'id_nat' : str(x.stock.producto.id) + "o" + str(x.stock.id) + "o" + str(x.stock.lote.id) + "o" + str(0),
				'presentacion_nat' : smart_str(x.stock.producto.presentacion_c),
				'lote_present_stock' : smart_str(x.stock.lote.nombre) + " / " + str(x.stock.cantidad - x.stock.cantidad_inversa) + " " + smart_str(x.stock.producto.presentacion_c) + "s",
				'nombre_nombrec' : smart_str(x.stock.producto.producto) + " / " + smart_str(x.stock.producto.nombre_comercial),
				'stock_id' : str(x.stock.id),
				'stock_cantidad' : str(x.stock.cantidad - x.stock.cantidad_inversa),
				'stock_granel_id' : str(0),
				'stock_granel_cantidad' : str(0),
				'lote_id' : str(x.stock.lote.id),
				'lote_nombre' : smart_str(x.stock.lote.nombre),
				'presentacion' : smart_str(x.stock.producto.presentacion_c),
				'precio' : str("%.2f" % float(x.stock.precio)),
				'nombre' : smart_str(x.stock.producto.producto),
				'nombrec' : smart_str(x.stock.producto.nombre_comercial),
				'button-add' : "<a href='#' class='button plain plain-blue mko_add-product super-small'><span class='glyph plus2'></span></a>",
			})
			
			for y in StockGranelForFront.objects.filter(stockforfront = x):
				_tmp.append({
					'id_nat' : str(y.stockforfront.stock.producto.id) + "o" + str(x.stock.id) + "o" + str(x.stock.lote.id) + "o" + str(x.id),
					'presentacion_nat' : smart_str(x.stock.producto.presentacion_u),
					'lote_present_stock' : smart_str(x.stock.lote.nombre) + " / " + str(x.cantidad - x.cantidad_inversa)  + " " + smart_str(x.stock.producto.presentacion_u) + "s",
					'nombre_nombrec' : smart_str(x.stock.producto.producto) + " / " + smart_str(x.stock.producto.nombre_comercial),
					'stock_id' : str(0),
					'stock_cantidad' : str(0),
					'stock_granel_id' : str(x.id),
					'stock_granel_cantidad' : str(x.cantidad - x.cantidad_inversa),
					'lote_id' : str(x.stock.lote.id),
					'lote_nombre' : smart_str(x.stock.lote.nombre),
					'presentacion' : smart_str(x.stock.producto.presentacion_u),
					'precio' : str("%.2f" % float(x.stock.precio)),
					'nombre' : smart_str(x.stock.producto.producto),
					'nombrec' : smart_str(x.stock.producto.nombre_comercial),
					'button-add' : "<a href='#' class='button plain plain-blue mko_add-product super-small'><span class='glyph plus2'></span></a>",
				})
				
		for x in StockGranelOfTemp.objects.filter(frontdesk = LocalFront):
			print x
			_tmp.append({
				'id_nat' : str(x.stock.stock.producto.id) + "o" + str(x.stock.id) + "o" + str(x.stock.stock.lote.id) + "o" + str(0),
				'presentacion_nat' : smart_str(x.stock.stock.producto.presentacion_c),
				'lote_present_stock' : smart_str(x.stock.stock.lote.nombre) + " / " + str(x.stock.cantidad - x.stock.cantidad_inversa) + " " + smart_str(x.stock.stock.producto.presentacion_c) + "s",
				'nombre_nombrec' : smart_str(x.stock.stock.producto.producto) + " / " + smart_str(x.stock.stock.producto.nombre_comercial),
				'stock_id' : str(x.stock.id),
				'stock_cantidad' : str(x.stock.stock.cantidad - x.stock.stock.cantidad_inversa),
				'stock_granel_id' : str(0),
				'stock_granel_cantidad' : str(0),
				'lote_id' : str(x.stock.stock.lote.id),
				'lote_nombre' : smart_str(x.stock.stock.lote.nombre),
				'presentacion' : smart_str(x.stock.stock.producto.presentacion_c),
				'precio' : str("%.2f" % float(x.stock.stock.precio)),
				'nombre' : smart_str(x.stock.stock.producto.producto),
				'nombrec' : smart_str(x.stock.stock.producto.nombre_comercial),
				'button-add' : "<a href='#' class='button plain plain-blue mko_add-product super-small'><span class='glyph plus2'></span></a>",
			})

				
		return HttpResponse(simplejson.dumps({"aaData":_tmp}), mimetype='application/json')
	return HttpResponse(simplejson.dumps({"status" : "0", "msg" : "Inicie sesión."}), mimetype='application/json')

def bk_venta(request, local=""):
	usuario = request.user
	if not (isLogin(usuario)):
		return HttpResponse(simplejson.dumps({"status" : "0", "msg" : "Inicie sesión."}), mimetype='application/json')
	if (okPermission(usuario.groups.all(), 1) or okPermission(usuario.groups.all(), 2) or okPermission(usuario.groups.all(), 3)):
		if request.method == "POST":
			i_Productos = []
			i_Detalle = []
			i_Total = 0
			#####
			## Formateamos el dato
			#####
			for x in request.POST['productos'].split(','): # Primero por comas
				if x:
					xcoma = x.split('-')
					xo = xcoma[0].split('o')
					detalleF = DetalleForm({'producto': xo[0], 'cantidad': xcoma[1], 'contado' : 'F'})
					if detalleF.is_valid():
						try:
							detalleF.save()
						except Exception, e:
							error = traceback.extract_stack()[-1]
							print (str(datetime.datetime.now()) + " - " + ", ".join([type(e).__name__, str(error[0]), str(error[2]), str(error[1])]))

					if xo[3] != '0':
						granel = StockGranel.objects.get(id = xo[3])
						granel.cantidad_inversa = granel.cantidad_inversa + int(xcoma[1])
						try:
							granel.save()
						except Exception, e:
							error = traceback.extract_stack()[-1]
							print (str(datetime.datetime.now()) + " - " + ", ".join([type(e).__name__, str(error[0]), str(error[2]), str(error[1])]))
						i_Total += granel.precio
					else:
						stocke = Stock.objects.get(id = xo[1])
						stocke.cantidad_inversa = stocke.cantidad_inversa + int(xcoma[1])
						try:
							stocke.save()
						except Exception, e:
							error = traceback.extract_stack()[-1]
							print (str(datetime.datetime.now()) + " - " + ", ".join([type(e).__name__, str(error[0]), str(error[2]), str(error[1])]))						
						i_Total += stocke.precio
					i_Detalle.append(detalleF.save().id)
			#####
			## End Formateamos el dato
			#####

			#####
			## Generamos el reporte de la venta
			#####
			VentaF = VentaForm({
				'detalle' : i_Detalle,
				'ruc' : '0',
				'cancelado' : 'F',
				'contado' : 'F',
				'monto' : i_Total,
				'igv' : 0,
				'boleta' : '0',
				'cliente' : 'Desconocido',
				'emision' : datetime.datetime.now(),
				'usuariox' : request.user.id
			})
			if VentaF.is_valid():
				try:
					VentaF.save()
				except Exception, e:
					error = traceback.extract_stack()[-1]
					print (str(datetime.datetime.now()) + " - " + ", ".join([type(e).__name__, str(error[0]), str(error[2]), str(error[1])]))

		return HttpResponse(simplejson.dumps({"status" : "1"}), mimetype='application/json')
	logear(str(datetime.datetime.now()) + " - bk_venta" + " - " + str(usuario) + " Denegación de servicios.")
	return HttpResponse(simplejson.dumps({"status" : "0", "msg" : "No tiene permisos para esta operación."}), mimetype='application/json')
