# -*- coding: utf-8 -*-
from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User, UserManager, Group
from django.utils.encoding import smart_str, smart_unicode
import datetime

## Modelo de la aplicación
class Aplicacion(models.Model):
	titulo = models.CharField(max_length = 100, verbose_name = "Titulo de la aplicación", help_text = "Ingrese el titulo de la aplicación", null = False, blank = False)
	documentacion = models.BooleanField(default = True, verbose_name = "Documentación", help_text = "Documentación.", null = False, blank = False)
	cierre_caja = models.TimeField(verbose_name = "Hora para el cierre de caja.", default = datetime.datetime.now())
	numerofactura = models.PositiveIntegerField(verbose_name = "Posición", help_text = "Ingrese un número para el orden.", default=1, null = False, blank = False)
	numeroboleta = models.PositiveIntegerField(verbose_name = "Posición", help_text = "Ingrese un número para el orden.", default=1, null = False, blank = False)

	def __unicode__(self):
		return u'%s' % (self.titulo)

class MenuFirstLevel(models.Model):
	grupo = models.ForeignKey(Group, verbose_name = "Grupo")
	titulo = models.CharField(max_length = 50, verbose_name = "Titulo de la opción", help_text = "Ingrese el titulo de la opción.", null = False, blank = False)
	disponible = models.BooleanField(default = True, verbose_name = "Disponible", help_text = "Indique la disponibilidad.", null = False, blank = False)
	orden = models.PositiveIntegerField(verbose_name = "Posición", help_text = "Ingrese un número para el orden.", null = False, blank = False)
	link = models.CharField(max_length = 250, verbose_name = "Enlace de la opción", help_text = "Ingrese el enlace de la opción (URL)", null = False, blank = False)
	attr_class = models.CharField(max_length = 50, verbose_name = "Atrubuto Class", help_text = "Ingrese el atributo Class.", null = False, blank = True)
	icono = models.CharField(max_length = 50, verbose_name = "Icono", help_text = "Ingrese el icono de la opcion.", null = False, blank = True)
	def __unicode__(self):
		return u'%s' % (str(self.orden) + "-" + self.titulo)
		
class MenuSecondLevel(models.Model):
	permisos = models.ForeignKey(Group, verbose_name = "Permisos")
	titulo = models.CharField(max_length = 50, verbose_name = "Titulo de la opción", help_text = "Ingrese el titulo de la opción.", null = False, blank = False)
	disponible = models.BooleanField(default = True, verbose_name = "Disponible", help_text = "Indique la disponibilidad.", null = False, blank = False)
	orden = models.PositiveIntegerField(verbose_name = "Posición", help_text = "Ingrese un número para el orden.", null = False, blank = False)
	link = models.CharField(max_length = 250, verbose_name = "Enlace de la opción", help_text = "Ingrese el enlace de la opción (URL)", null = False, blank = False)
	def __unicode__(self):
		return u'%s' % (str(self.id) + " - " + self.titulo)
		
class MenuThirdLevel(models.Model):
	permisos = models.ForeignKey(Group, verbose_name = "Permisos")
	titulo = models.CharField(max_length = 50, verbose_name = "Titulo de la opción", help_text = "Ingrese el titulo de la opción.", null = False, blank = False)
	disponible = models.BooleanField(default = True, verbose_name = "Disponible", help_text = "Indique la disponibilidad.", null = False, blank = False)
	orden = models.PositiveIntegerField(verbose_name = "Posición", help_text = "Ingrese un número para el orden.", null = False, blank = False)
	link = models.CharField(max_length = 250, verbose_name = "Enlace de la opción", help_text = "Ingrese el enlace de la opción (URL)", null = False, blank = False)
	def __unicode__(self):
		return u'%s' % (self.titulo)

class Laboratorio(models.Model):
	nombre = models.CharField(max_length = 250, verbose_name = "Laboratorio", help_text = "Ingrese el nombre del Laboratorio", null = False, blank = False)
	def __unicode__(self):
		return u'%s' % (self.nombre)

class Componente(models.Model):
	nombre = models.CharField(max_length = 250, verbose_name = "Compuesto", help_text = "Ingrese el nombre del Compuesto", null = False, blank = False)
	def __unicode__(self):
		return u'%s' % (self.nombre)
		
class Presentacion(models.Model):
	nombre = models.CharField(max_length = 250, verbose_name = "Presentación", help_text = "Ingrese el nombre de la Presentación", null = False, blank = False)
	def __unicode__(self):
		return u'%s' % (self.nombre)
		
class Proveedor(models.Model):
	ruc = models.CharField(max_length = 25, verbose_name = "Ruc del Proveedor", help_text = "Ingrese el Ruc del Proveedor", null = False, blank = False)
	nombre = models.CharField(max_length = 255, verbose_name = "Nombre del Proveedor", help_text = "Ingrese el nombre del Proveedor", null = False, blank = False)
	contacto = models.CharField(max_length = 255, verbose_name = "Nombre del Contacto", help_text = "Ingrese el nombre del Contacto", null = False, blank = False)
	direccion = models.CharField(max_length = 255, verbose_name = "Dirección", help_text = "Ingrese la dirección del Proveedor", null = False, blank = False)
	telefono = models.CharField(max_length = 20, verbose_name = "Teléfono del Proveedor", help_text = "Ingrese el teléfono del Proveedor", null = False, blank = False)
	movil = models.CharField(max_length = 20, verbose_name = "Móvil del Proveedor", help_text = "Ingrese el móvil del Proveedor", null = False, blank = False)
	rpmrpc = models.CharField(max_length = 20, verbose_name = "RPM/RPC del Proveedor", help_text = "Ingrese el RPM/RPC del Proveedor", null = False, blank = False)
	email = models.EmailField(verbose_name = "Email del Proveedor", help_text = "Ingrese el Email del Proveedor", null = False, blank = False)
	def __unicode__(self):
		return u'%s' % (self.nombre)

class Malestar(models.Model):
	nombre = models.CharField(max_length = 250, verbose_name = "Malestar", help_text = "Ingrese el nombre del Malestar", null = False, blank = False)
	def __unicode__(self):
		return u'%s' % (self.nombre)

# Usuariox
class Usuariox(User):
	telfijo = models.CharField(max_length = 15, verbose_name = "Teléfono fijo", help_text = "Ingrese el teléfono fijo.", null = False, blank = False)
	telmobil = models.CharField(max_length = 15, verbose_name = "Teléfono móbil", help_text = "Ingrese el teléfono móvil.", null = False, blank = False)
	objects = UserManager()
	def __unicode__(self):
		return  u'%s' % (self.username + " - " + self.last_name + " " + self.first_name)
	
	def is_in_group(self, uu):
		for uu in self.groups.all():
			if uu.id == patron:
				return True
		return False
		
class Lote(models.Model):
	nombre = models.CharField(max_length = 250, verbose_name = "Nombre del Lote", help_text = "Ingrese el nombre del lote.", null = False, blank = False)
	descripcion = models.TextField(max_length = 1000, verbose_name = "Descripción del Lote", help_text = "Ingrese la descripción del lote.", null = False, blank = False)
	fecha_entrada = models.DateField(verbose_name = "Hora de Inscripción", default = datetime.datetime.now(), null = False, blank = False)
	proveedor = models.ForeignKey(Proveedor, verbose_name = "Seleccione el Proveedor")
	def __unicode__(self):
		return  u'%s' % (self.nombre + " - "  + str(self.fecha_entrada))

class Producto(models.Model):
	#**********************
		# Esta Clase representa a una caja de productos
		# los cuales tienen varias unidades de producto
		# El precio detalla, el valor que tiene cada caja
		# La cantidad detalla, la cantidad de producto que tiene una caja
	#***********************
	producto = models.CharField(max_length = 250, verbose_name = "Nombre", help_text = "Ingrese el nombre del Producto.", null = False, blank = False)
	nombre_comercial = models.CharField(max_length = 250, verbose_name = "Nombre Comercial", help_text = "Ingrese el nombre comercial del Producto.", null = False, blank = False)
	RECETA = [['T', 'Si'], ['F','No']]
	receta_medica = models.CharField(max_length = 1, verbose_name = "Receta Médica", help_text = "¿Este medicamento se vender solo si se presenta reseta médica?", choices = RECETA, null = False, blank = False)
	compuesto = models.ManyToManyField(Componente, verbose_name = "Seleccione los componentes")
	laboratorio = models.ForeignKey(Laboratorio, verbose_name = "Seleccione el laboratorio")
	presentacion_u = models.ForeignKey(Presentacion, related_name='presentacion_en_unidad', verbose_name = "Seleccione la presentación de la unidad")
	presentacion_c = models.ForeignKey(Presentacion, related_name='presentacion_en_cantidad', verbose_name = "Seleccione la presentación en cantidad")
	malestares = models.ManyToManyField(Malestar, verbose_name = "Seleccione los malestares")
	recomendaciones = models.TextField(verbose_name = "Recomendaciones", help_text = "Ingrese las recomendaciones para este medicamento", null = False, blank = False)
	contraindicaciones = models.TextField(verbose_name = "Contraindicaciones", help_text = "Ingrese las contraindicaciones para este medicamento", null = False, blank = False)
	usuariox = models.ForeignKey(Usuariox, verbose_name = "Usuario")
	def __unicode__(self):
		return u'%s' % (self.nombre)
	def r_compuesto(self):
		return self.compuesto.all()[:2]
	def r_malestares(self):
		return self.malestares.all()[:2]
	def r_presentacion_u(self):
		return self.presentacion_u.nombre
	def r_presentacion_c(self):
		return self.presentacion_c.nombre
	def __unicode__(self):
		return  u'%s' % (self.producto)
	def r_stock(self):
		return Stock.objects.filter(producto = self)

		
class FrontDesk(models.Model):
	nombre = models.CharField(max_length = 250, verbose_name = "Nombre del Front Desk", help_text = "Ingrese el nombre del Front Desk.", null = False, blank = False)
	descripcion = models.TextField(max_length = 1000, verbose_name = "Descripción del FronDesk", help_text = "Ingrese la descripción del FronDesk.", null = False, blank = False)
	TF = [['T', 'Si'], ['F','No']]
	canstock = models.CharField(max_length = 1, verbose_name = "Vende por mayor", choices = TF, default = "T", blank = False, null = False)
	cangranel = models.CharField(max_length = 1, verbose_name = "Vende a granel", choices = TF, default = "T", blank = False, null = False)
	usuariox = models.ManyToManyField(Usuariox, verbose_name = "Usuario")
	def __unicode__(self):
		return  u'%s' % (self.nombre)
		
	def get_FrontByUser(self, usuario):
		return [{'id' : str(x['id']), 'nombre' : smart_str(x['nombre']) }  for x in FrontDesk.objects.filter(usuariox = usuario).values('id', 'nombre')]
###########
## Sotcks
###########
class Stock(models.Model):
	lote = models.ForeignKey(Lote, verbose_name = "Seleccione el Lote")
	producto = models.ForeignKey(Producto, verbose_name = "Seleccione el Producto")
	cantidad = models.IntegerField(verbose_name = "Cantidad de Unidades", help_text = "Ingrese la cantidad de unidades.", null = False, blank = False)
	cantidad_inversa = models.IntegerField(verbose_name = "Cantidad de Unidades Consumidas", help_text = "Ingrese la cantidad de unidades consumidas.", null = False, default = 0, blank = False)
	cantidad_cu = models.IntegerField(verbose_name = "Cantidad por cada Unidad", help_text = "Ingrese la cantidad por unidade.", null = False, blank = False)
	precio = models.DecimalField(verbose_name = "Precio de Unidad", max_digits = 15, decimal_places = 3, help_text = "Ingrese el precio de unidad.", blank = False, null = False)
	fecha_vencimiento = models.DateField(verbose_name = "Fecha de vencimiento", default = datetime.datetime.now(), null = False, blank = False)	
	usuariox = models.ForeignKey(Usuariox, verbose_name = "Usuario")
	def __unicode__(self):
		return  u'%s' % (str(self.lote) + " - " + str(self.producto))
	
	def get_total(self):
		return self.cantidad - self.cantidad_inversa
	
class StockUnidadTemp(models.Model):
	stock = models.ForeignKey(Stock, verbose_name = "Stock")
	cantidad = models.IntegerField(verbose_name = "Cantidad de Unidades", help_text = "Ingrese la cantidad de unidades.", null = False, blank = False)
	cantidad_inversa = models.IntegerField(verbose_name = "Cantidad de Unidades Consumidas", help_text = "Ingrese la cantidad de unidades consumidas.", default = 0, null = False, blank = False)
	precio = models.DecimalField(verbose_name = "Precio de Unidad", max_digits = 15, decimal_places = 3, help_text = "Ingrese el precio de unidad.", blank = False, null = False)
	usuariox = models.ForeignKey(Usuariox, verbose_name = "Usuario")
	def __unicode__(self):
		return  u'%s' % (str(self.stock.lote) + " - " + str(self.stock.producto) + " - " + str(self.cantidad))
	
	def get_total(self):
		return self.cantidad - self.cantidad_inversa
		
class StockUnidad(models.Model):
	lote = models.ForeignKey(Lote, verbose_name = "Seleccione el Lote")
	producto = models.ForeignKey(Producto, verbose_name = "Seleccione el Producto")
	cantidad = models.IntegerField(verbose_name = "Cantidad de Unidades", help_text = "Ingrese la cantidad de unidades.", null = False, blank = False)
	cantidad_inversa = models.IntegerField(verbose_name = "Cantidad de Unidades Consumidas", help_text = "Ingrese la cantidad de unidades consumidas.", default = 0, null = False, blank = False)
	precio = models.DecimalField(verbose_name = "Precio de Unidad", max_digits = 15, decimal_places = 3, help_text = "Ingrese el precio de unidad.", blank = False, null = False)
	fecha_vencimiento = models.DateField(verbose_name = "Fecha de vencimiento", default = datetime.datetime.now(), null = False, blank = False)	
	usuariox = models.ForeignKey(Usuariox, verbose_name = "Usuario")
	def __unicode__(self):
		return  u'%s' % (str(self.lote) + " - " + str(self.producto))
		
###########
## Asignación
###########
class StockForFront(models.Model):
	stock = models.ForeignKey(Stock, verbose_name = "Stock")
	frontdesk = models.ForeignKey(FrontDesk, verbose_name = "FrontDesk")
	cantidad = models.IntegerField(verbose_name = "Cantidad de Unidades", help_text = "Ingrese la cantidad de unidades.", null = False, blank = False)
	cantidad_inversa = models.IntegerField(verbose_name = "Cantidad de Unidades Consumidas", help_text = "Ingrese la cantidad de unidades consumidas.", default = 0, null = False, blank = False)	
	def __unicode__(self):
		return  u'%s' % (str(self.stock.lote.nombre) + " - " + str(self.frontdesk) + " - " + str(self.cantidad) + " Unidades")

class StockGranelOfTemp(models.Model):
	stock = models.ForeignKey(StockUnidadTemp, verbose_name = "Seleccione el Stock")
	frontdesk = models.ForeignKey(FrontDesk, verbose_name = "FrontDesk")
	cantidad = models.IntegerField(verbose_name = "Cantidad de Unidades a Granel", help_text = "Ingrese la cantidad de unidades a granel.", null = False, blank = False)
	cantidad_inversa = models.IntegerField(verbose_name = "Cantidad de Unidades a Granel Consumidas", help_text = "Ingrese la cantidad de unidades a granel consumidas.", default = 0, null = False, blank = False)
	precio = models.DecimalField(verbose_name = "Precio de Unidad", max_digits = 15, decimal_places = 3, help_text = "Ingrese el precio de unidad.", blank = False, null = False)
	fecha_entrada = models.DateField(auto_now = True, verbose_name = "Hora de entrada del producto a granel", default = datetime.datetime.now(), null = False, blank = False)
	usuariox = models.ForeignKey(Usuariox, verbose_name = "Usuario")
	def __unicode__(self):
		return  u'%s' % (str(self.stock) + " " + str(self.stock.stock.producto))

class StockGranel(models.Model):
	stock = models.ForeignKey(StockUnidad, verbose_name = "Seleccione el Stock")
	frontdesk = models.ForeignKey(FrontDesk, verbose_name = "FrontDesk")
	cantidad = models.IntegerField(verbose_name = "Cantidad de Unidades a Granel", help_text = "Ingrese la cantidad de unidades a granel.", null = False, blank = False)
	cantidad_inversa = models.IntegerField(verbose_name = "Cantidad de Unidades a Granel Consumidas", help_text = "Ingrese la cantidad de unidades a granel consumidas.", default = 0, null = False, blank = False)
	precio = models.DecimalField(verbose_name = "Precio de Unidad", max_digits = 15, decimal_places = 3, help_text = "Ingrese el precio de unidad.", blank = False, null = False)
	fecha_entrada = models.DateField(auto_now = True, verbose_name = "Hora de entrada del producto a granel", default = datetime.datetime.now(), null = False, blank = False)
	usuariox = models.ForeignKey(Usuariox, verbose_name = "Usuario")
	def __unicode__(self):
		return  u'%s' % (str(self.stock) + " " + str(self.stock.producto))
###########
## Distribucion
###########
class StockGranelForFront(models.Model):
	stockforfront = models.ForeignKey(StockForFront, verbose_name = "Seleccione el Stock")
	cantidad = models.IntegerField(verbose_name = "Cantidad de Unidades a Granel", help_text = "Ingrese la cantidad de unidades a granel.", null = False, blank = False)
	cantidad_inversa = models.IntegerField(verbose_name = "Cantidad de Unidades a Granel Consumidas", help_text = "Ingrese la cantidad de unidades a granel consumidas.", default = 0, null = False, blank = False)
	precio = models.DecimalField(verbose_name = "Precio de Unidad", max_digits = 15, decimal_places = 3, help_text = "Ingrese el precio de unidad.", blank = False, null = False)
	fecha_entrada = models.DateField(verbose_name = "Hora de entrada del producto a granel", default = datetime.datetime.now(), null = False, blank = False)
	usuariox = models.ForeignKey(Usuariox, verbose_name = "Usuario")
	def __unicode__(self):
		return  u'%s' % (str(self.stockforfront) + " " + str(self.stockforfront.stock) + " " + str(self.stockforfront.stock.producto))
###########
## Venta
###########
class Detalle(models.Model):
	producto = models.ForeignKey(Producto, verbose_name = "Producto")
	cantidad = models.IntegerField(verbose_name = "Cantidad", help_text = "Ingrese la cantidad.", null = False, blank = False)
	contado = models.CharField(max_length = 1, verbose_name = "Constado", default = "F", blank = False, null = False)
	def __unicode__(self):
		return u'%s' % (self.producto)
	
class Venta(models.Model):
	detalle =  models.ManyToManyField(Detalle, verbose_name = "Detalle")
	horaregistro = models.DateTimeField(auto_now = True, verbose_name = "Hora de registro.", default = datetime.datetime.now())
	ruc = models.CharField(max_length = 15, verbose_name = "Ruc", help_text = "Ingrese el Ruc de la venta", null = False, blank = True)
	cancelado = models.CharField(max_length = 1, verbose_name = "Estado", default = "F", blank = False, null = False)	# True o False (De acuerdo a la situacion)
	contado = models.CharField(max_length = 1, verbose_name = "Cerrado", default = "F", blank = False, null = False)
	monto = models.DecimalField(verbose_name = "Monto", max_digits = 15, decimal_places = 3, help_text = "Ingrese el Monto Pagado.", blank = False, null = False)
	igv =  models.DecimalField(verbose_name = "IGV", max_digits = 15, decimal_places = 3, help_text = "Ingrese el IGV.", default=0, null = False, blank = False)
	boleta = models.IntegerField(verbose_name = "Boleta", help_text = "Ingrese un número de boleta.", null = False, blank = False)
	ruc = models.IntegerField(verbose_name = "RUC", help_text = "Ingrese un número de Ruc del cliente.", null = False, blank = False)
	cliente = models.CharField(max_length = 250, verbose_name = "Persona que paga", help_text = "Nombre de la persona que paga", default = "Desconocido", null = False, blank = False)
	emision = models.DateTimeField(verbose_name = "Hora de Emisión", default = datetime.datetime.now(), null = False, blank = False)
	usuariox = models.ForeignKey(Usuariox, verbose_name = "Usuario")
	def __unicode__(self):
		return u'%s' % (self.emision)
	def r_totalmonto(self):
		return self.objects.aggregate(sum('monto'))
	def r_top(self, limit = 0):
		if limit:
			return self.objects().order_by('emision')[:limit]
		else:
			return self.objects().order_by('emision')[:10]
	def r_detalle(self):
		return [str(x.producto) + " - " + str(x.cantidad) for x in self.detalle.all()]

class Farmacia(models.Model):
	lote = models.ManyToManyField(Lote, verbose_name = "Seleccione los Lotes")
	
	def __unicode__(self):
		return u'%s' % (self.id)
