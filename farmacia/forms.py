#encoding:utf-8

from django import forms
from django.forms import ModelMultipleChoiceField
from django.forms.fields import MultipleChoiceField
from django.forms.widgets import SelectMultiple, PasswordInput, SplitDateTimeWidget, HiddenInput, Textarea, MultipleHiddenInput
from django.db import models
from farmacia.models import Usuariox
class LoginForm(forms.Form):
	username = forms.CharField(label = "DNI", max_length = 30)
	password = forms.CharField(label = "Contaseña", widget = forms.PasswordInput)

#Productos
from farmacia.models import Laboratorio
class LaboratorioForm(forms.ModelForm):
	class Meta:
		model = Laboratorio
from farmacia.models import Lote
class LoteForm(forms.ModelForm):
	class Meta:
		model = Lote
from farmacia.models import Componente
class ComponenteForm(forms.ModelForm):
	class Meta:
		model = Componente
from farmacia.models import Malestar
class MalestarForm(forms.ModelForm):
	class Meta:
		model = Malestar
		
from farmacia.models import Producto
class ProductoForm(forms.ModelForm):
	class Meta:
		model = Producto
		
from farmacia.models import Detalle
class DetalleForm(forms.ModelForm):
	class Meta:
		model = Detalle
from farmacia.models import Venta
class VentaForm(forms.ModelForm):
	class Meta:
		model = Venta
from farmacia.models import Stock
class StockForm(forms.ModelForm):
	class Meta:
		model = Stock
from farmacia.models import StockGranel
class StockGranelForm(forms.ModelForm):
	class Meta:
		model = StockGranel

from farmacia.models import Farmacia
class FarmaciaForm(forms.ModelForm):
	class Meta:
		model = Farmacia
		
from farmacia.models import Presentacion
class PresentacionForm(forms.ModelForm):
	class Meta:
		model = Presentacion

from farmacia.models import Stock
class StockForm(forms.ModelForm):
	class Meta:
		model = Stock
		widgets = {
			'cantidad_inversa' : forms.TextInput(attrs={'readonly':'true', 'class':'readonly'}),
		}
				
class StockFormEdit(forms.ModelForm):
	class Meta:
		model = Stock
		widgets = {
			'cantidad' : forms.TextInput(attrs={'readonly':'true', 'class':'readonly'}),
			'cantidad_inversa' : forms.TextInput(attrs={'readonly':'true', 'class':'readonly'}),
		}		
		
from farmacia.models import FrontDesk
class FrontDeskForm(forms.ModelForm):
	class Meta:
		model = FrontDesk
###########
## Sotcks
###########
from farmacia.models import StockUnidad
class StockUnidadForm(forms.ModelForm):
	class Meta:
		model = StockUnidad
		widgets = {
			'cantidad_inversa' : forms.TextInput(attrs={'readonly':'true', 'class':'readonly'}),
		}

class StockUnidadFormEdit(forms.ModelForm):
	class Meta:
		model = StockUnidad
		widgets = {
			'cantidad' : forms.TextInput(attrs={'readonly':'true', 'class':'readonly'}),
			'cantidad_inversa' : forms.TextInput(attrs={'readonly':'true', 'class':'readonly'}),
		}
		
from farmacia.models import StockUnidadTemp
class StockUnidadTempForm(forms.ModelForm):
	def __init__(self, maximo, *args, **kwargs):
		super(StockUnidadTempForm, self).__init__(*args, **kwargs) 
		self.fields['cantidad'].widget.attrs['class']= '{validate:{required:true, max:' + str(maximo) + '}}'
		self.fields['cantidad'].label = "Cuantas caja abrira"
	class Meta:
		model = StockUnidadTemp
		
class StockUnidadTempEditForm(forms.ModelForm):
	class Meta:
		model = StockUnidadTemp
		widgets = {
			'cantidad' : forms.TextInput(attrs={'readonly':'true'}),
			'precio' : forms.TextInput(attrs={'readonly':'true'}),
		}
		
from farmacia.models import StockGranel
class StockGranelForm(forms.ModelForm):
	class Meta:
		model = StockGranel		
###########
## Asignación
###########
from farmacia.models import StockForFront
class StockForFrontForm(forms.ModelForm):
	def __init__(self, maximo, *args, **kwargs):
		super(StockForFrontForm, self).__init__(*args, **kwargs) 
		self.fields['cantidad'].widget.attrs['class']= '{validate:{required:true, max:' + str(maximo) + '}}'
	class Meta:
		model = StockForFront
		widgets = {
			'cantidad_inversa' : forms.TextInput(attrs={'readonly':'true', 'class':'readonly'}),
		}

from farmacia.models import StockGranelOfTemp
class StockGranelOfTempForm(forms.ModelForm):
	def __init__(self, maximo, *args, **kwargs):
		super(StockGranelOfTempForm, self).__init__(*args, **kwargs) 
		self.fields['cantidad'].widget.attrs['class']= '{validate:{required:true, max:' + str(maximo) + '}}'
	class Meta:
		model = StockGranelOfTemp
		widgets = {
			'cantidad_inversa' : forms.TextInput(attrs={'readonly':'true', 'class':'readonly'}),
		}
	
#class StockUnidadTempForm(forms.ModelForm):
#	class Meta:
#		model = StockUnidadTemp


		


from farmacia.models import Usuariox
class UsuarioxForm(forms.ModelForm):
	class Meta:
		model = Usuariox
		exclude = ['is_active', 'user_permissions','is_staff','is_superuser','last_login','date_joined']
		
class UsuarioxFormPass(forms.ModelForm):
	class Meta:
		model = Usuariox
		exclude = ['is_staff', 'is_active', 'last_login', 'date_joined', 'is_superuser', 'user_permissions', 'username', 'first_name',
					'last_name', 'email', 'telfijo' ,'telmobil', 'groups']
