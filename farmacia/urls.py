#encoding:utf-8

from django.conf.urls.defaults import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'bona.views.home', name='home'),
    # url(r'^bona/', include('bona.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
	url(r'^chitadmin/', include(admin.site.urls)),
	# Aplicación
	#url(r'^$', 'farmacia.views.onlogout'),
	url(r'^login/$', 'farmacia.views.login'),
	url(r'^salir/$', 'farmacia.views.onlogout'),
	url(r'^usuario/$', 'farmacia.views.usuario'),
	
	url(r'^administrator/login/$', 'farmacia.views.login_admin'),
	url(r'^administrator/dashboard/$', 'farmacia.views.dash_admin'),
	#local
	url(r'^local/(?P<local>\d{1,})/$', 'farmacia.views.index_local'),
	url(r'^local/(?P<local>\d{1,})/opciones/$', 'farmacia.views.dashboard_local'),
	url(r'^local/(?P<local>\d{1,})/opciones/mi-stock/$', 'farmacia.views_stock.stock_local'),
		url(r'^local/(?P<local>\d{1,})/producto/bk/productos/$', 'farmacia.views_product.bk_productos_data'),
		url(r'^local/(?P<local>\d{1,})/producto/bk/productos/venta/$', 'farmacia.views_product.bk_venta'),
	# Configuracion
	url(r'^farmacia/configuracion/$', 'farmacia.views_configuration.configuration'),
	url(r'^farmacia/configuracion/(?P<codigo>\d{1,})/$', 'farmacia.views_configuration.configuration'),
	url(r'^farmacia/registro-por-pasos/(?P<parametro>.+)/$', 'farmacia.views_wizard.RWizard'),
	# Registros
	url(r'^administrator/farmacia/laboratorio/$', 'farmacia.views_laboratorio.RLaboratorio'),
	url(r'^administrator/farmacia/laboratorio/(?P<codigo>\d{1,})/$', 'farmacia.views_laboratorio.RLaboratorio'),
	url(r'^administrator/farmacia/stock/$', 'farmacia.views_stock.RStock'),
	url(r'^administrator/farmacia/stock/(?P<codigo>\d{1,})/$', 'farmacia.views_stock.RStock'),
	
	url(r'^administrator/farmacia/stock/(?P<codigo>\d{1,})/asignacion/$', 'farmacia.views_stock.RStockasignacion'),
		url(r'^administrator/bk/farmacia/stock/(?P<codigo>\d{1,})/asignacion/$', 'farmacia.views_stock.get_bk_Stock'),
	
	url(r'^administrator/farmacia/stock/(?P<codigo>\d{1,})/granel/$', 'farmacia.views_stock.RStockUnidadTemp'),
	
	url(r'^administrator/farmacia/stock/(?P<codigo>\d{1,})/granel/(?P<granel>\d{1,})/asignacion/','farmacia.views_stock.RStockUnidadTempasignacion'),
	
	url(r'^administrator/farmacia/stock-granel/$', 'farmacia.views_stockgranel.RStockUnidad'),
	url(r'^administrator/farmacia/stock-granel/(?P<codigo>\d{1,})/$', 'farmacia.views_stockgranel.RStockUnidad'),
	
	
	
	url(r'^administrator/farmacia/lote/$', 'farmacia.views_lote.RLote'),
	url(r'^administrator/farmacia/lote/(?P<codigo>\d{1,})/$', 'farmacia.views_lote.RLote'),
	
	url(r'^administrator/farmacia/compuesto/$', 'farmacia.views_componente.RComponente'),
	url(r'^administrator/farmacia/compuesto/(?P<codigo>\d{1,})/$', 'farmacia.views_componente.RComponente'),
	url(r'^administrator/farmacia/usuario/$', 'farmacia.views_usuariox.RUsuariox'),
	url(r'^administrator/farmacia/usuario/(?P<codigo>\d{1,})/$', 'farmacia.views_usuariox.RUsuariox'),
	url(r'^administrator/farmacia/usuario/(?P<codigo>\d{1,})/password/$', 'farmacia.views_usuariox.UsuarioxChangePass'),
	url(r'^administrator/farmacia/presentacion/$', 'farmacia.views_presentacion.RPresentacion'),
	url(r'^administrator/farmacia/presentacion/(?P<codigo>\d{1,})/$', 'farmacia.views_presentacion.RPresentacion'),
	url(r'^administrator/farmacia/accion-farmacologica/$', 'farmacia.views_malestar.RMalestar'),
	url(r'^administrator/farmacia/accion-farmacologica/(?P<codigo>\d{1,})/$', 'farmacia.views_malestar.RMalestar'),
	url(r'^administrator/farmacia/producto/$', 'farmacia.views_product.RProducto'),
	url(r'^administrator/farmacia/producto/(?P<codigo>\d{1,})/$', 'farmacia.views_product.RProducto'),
		url(r'^farmacia/productos_data/$', 'farmacia.views_product.bk_productos_data'),
		url(r'^farmacia/bk/contabilidad/venta/$', 'farmacia.views_product.bk_venta'),
	
	#url(r'^(?P<parametro>.+)$', 'farmacia.views.onlogout'),
	# Negocio
	#url(r'^opciones/lotes/$', 'farmacia.views_farmacia.lotes'),
	#url(r'^vender/$', 'farmacia.views.vender'),
	# Backends
)
