import os
import sys
import site
sys.path.append(os.path.abspath(os.path.dirname(__file__)))

sys.path.append(os.path.join(os.path.abspath(os.path.dirname(__file__)), '/home/chita/dj-mysql/lib/python2.6/site-packages'))

sys.path.insert(0, os.path.join(os.path.abspath(os.path.dirname(__file__)),'..'))

os.environ['DJANGO_SETTINGS_MODULE'] = 'makako.settings'
import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

